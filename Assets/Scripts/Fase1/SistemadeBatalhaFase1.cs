using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleStateF1 { START, PLAYERTURN, ENEMYTURN, WON, LOST }
public class SistemadeBatalhaFase1 : MonoBehaviour
{
    public BattleStateF1 state;

    [Header("Personagens")]
    public GameObject player;
    public GameObject enemy;

    [Header("Bot�o")]
    public GameObject button;
    private bool showButton;
    public GameObject buttonCure;
    private bool showButtonCure;
    public GameObject sE;

    [Header("HUD")]
    [SerializeField]
    CharHUD playerHUD;
    [SerializeField]
    CharHUD enemyHUD;
    public Text dialogueText;
    public GameObject imageSE;
    public GameObject tP1;

    [Header("Telas")]
    public GameObject telaLost;
    public GameObject telaWon;

    [Header("Sons")]
    public AudioSource[] Som;

    Personagens playerCharacter;
    Personagens enemyCharacter;

    public bool selectedEnemy;

    // Start is called before the first frame update
    void Start()
    {
        state = BattleStateF1.START;
        StartCoroutine(SetupBattle());

        showButton = false;
        showButtonCure = false;
        selectedEnemy = false;
        telaWon.SetActive(false);
        telaLost.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        playerHUD.SetHUD(playerCharacter);
        enemyHUD.SetHUD(enemyCharacter);

        if (state == BattleStateF1.PLAYERTURN && showButton)
        {
            button.SetActive(true);
        }
        else
        {
            button.SetActive(false);
        }

        if (state == BattleStateF1.PLAYERTURN && showButtonCure)
        {
            buttonCure.SetActive(true);
        }
        else
        {
            buttonCure.SetActive(false);
        }

        if (state == BattleStateF1.PLAYERTURN && selectedEnemy)
        {
            imageSE.SetActive(true);
        }
        else
        {
            imageSE.SetActive(false);
        }

        if (selectedEnemy)
        {
            showButton = true;
        }
        else
        {
            showButton = false;
        }

        Turn();
    }

    IEnumerator SetupBattle()
    {
        GameObject playerGO = player;
        playerCharacter = playerGO.GetComponent<Personagens>();
        GameObject enemyGO = enemy;
        enemyCharacter = enemyGO.GetComponent<Personagens>();

        dialogueText.text = "VEJA! � UMA DAS C�LULAS ALTERADAS, ELA EST�... ESTRANHA. CUIDADO AO CHEGAR PERTO, ELAS COSTUMAM ATACAR";

        yield return new WaitForSeconds(5f);

        state = BattleStateF1.PLAYERTURN;
        PlayerTurn();
    }

    void PlayerTurn()
    {

        if (playerCharacter.atualHp < playerCharacter.maxHp)
        {
            showButtonCure = true;
            dialogueText.text = "CASO DECIDA SE CURAR, SELECIONE O �CURAR� DURANTE SEU TURNO";
        }
        else
        {
            showButtonCure = false;
            dialogueText.text = "SEU TURNO, SELECIONE SEU INIMIGO E PARA ATACAR, SELECIONE O �ATACAR� DURANTE SEU TURNO";
        }
    }

    public void SelecionarInimigo()
    {
        if (state == BattleStateF1.PLAYERTURN)
        {
            selectedEnemy = true;
            showButton = true;
            dialogueText.text = "ATAQUE";
            Som[0].Play();
        }
    }

    public void Toque()
    {
        showButton = false;
        if (state != BattleStateF1.PLAYERTURN)
        {
            return;
        }
        StartCoroutine(PlayerAttack());
    }

    public void ToqueCura()
    {
        if (state != BattleStateF1.PLAYERTURN)
        {
            return;
        }
        StartCoroutine(PlayerHeal());
    }

    IEnumerator PlayerHeal()
    {
        selectedEnemy = false;
        showButton = false;
        showButtonCure = false;
        playerCharacter.Cura(7);
        Som[2].Play();
        playerHUD.SetHP(playerCharacter.atualHp);
        dialogueText.text = "INCRIVEL, VOC� SE RECUPEROU";

        yield return new WaitForSeconds(2f);

        state = BattleStateF1.ENEMYTURN;
        StartCoroutine(EnemyTurn());
    }

    IEnumerator PlayerAttack()
    {
        selectedEnemy = false;
        showButton = false;
        showButtonCure = false;
        Som[1].Play();
        bool isDead = enemyCharacter.Dano(playerCharacter.dano);
        dialogueText.text = "UAU, QUE ATAQUE PODEROSO";
        //enemyHud.SetHP(enemyCharacter.currentHp);
        yield return new WaitForSeconds(3f);

        if (isDead)
        {
            state = BattleStateF1.WON;
            StartCoroutine(EndBattle());
        }
        else
        {
            state = BattleStateF1.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }

    IEnumerator EnemyTurn()
    {
        Som[3].Play();
        dialogueText.text = "TURNO DO INIMIGO";
        bool isDead = playerCharacter.Dano(enemyCharacter.dano);
        //playerHud.SetHP(playerCharacter.currentHp);
        yield return new WaitForSeconds(2f);

        if (isDead)
        {
            state = BattleStateF1.LOST;
            StartCoroutine(EndBattle());
        }
        else
        {
            state = BattleStateF1.PLAYERTURN;
            PlayerTurn();
            showButton = true;
        }
    }

    public void Turn()
    {
        if (state == BattleStateF1.PLAYERTURN)
        {
            tP1.SetActive(true);
        }
        else
        {
            tP1.SetActive(false);
        }
    }

    IEnumerator EndBattle()
    {
        if (state == BattleStateF1.WON)
        {
            dialogueText.text = "VOC� VENCEU, TUTORIAL FINALIZADO";
            yield return new WaitForSeconds(2f);
            if(FasesManager.fase == 1)
            {
                FasesManager.fase = 2;
                PlayerPrefs.SetInt("fase", FasesManager.fase);
            }
            telaWon.SetActive(true);
        }
        else if (state == BattleStateF1.LOST)
        {
            dialogueText.text = "VOC� PERDEU, TENTE NOVAMENTE";
            yield return new WaitForSeconds(2f);
            telaLost.SetActive(true);
        }
    }
}
