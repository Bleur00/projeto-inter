using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleState { START, PLAYERTURN, ENEMYTURN, WON, LOST }
public class SistemadeBatalha : MonoBehaviour
{
    public BattleState state;

    [Header("Personagens")]
    public GameObject player;
    public GameObject enemy;

    [Header("Bot�o")]
    public GameObject button;
    private bool showButton;
    public GameObject buttonCure;
    private bool showButtonCure;
    public GameObject sE;

    [Header("HUD")]
    [SerializeField]
    CharHUD playerHUD;
    [SerializeField]
    CharHUD enemyHUD;
    public Text dialogueText;
    public GameObject imageSE;
    public GameObject tP1;

    [Header("Telas")]
    public GameObject telaLost;
    public GameObject telaWon;

    [Header("Sons")]
    public AudioSource[] Som;

    Personagens playerCharacter;
    Personagens enemyCharacter;

    public bool selectedEnemy;

    // Start is called before the first frame update
    void Start()
    {
        dialogueText.text = "INICIO";
        state = BattleState.START;
        StartCoroutine(SetupBattle());

        showButton = false;
        showButtonCure = false;
        selectedEnemy = false;

        telaWon.SetActive(false);
        telaLost.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        playerHUD.SetHUD(playerCharacter);
        enemyHUD.SetHUD(enemyCharacter);

        if (state == BattleState.PLAYERTURN && showButton)
        {
            button.SetActive(true);
        }
        else
        {
            button.SetActive(false);
        }

        if (state == BattleState.PLAYERTURN && showButtonCure)
        {
            buttonCure.SetActive(true);
        }
        else
        {
            buttonCure.SetActive(false);
        }

        if (state == BattleState.PLAYERTURN && selectedEnemy)
        {
            imageSE.SetActive(true);
        }
        else
        {
            imageSE.SetActive(false);
        }

        if (selectedEnemy)
        {
            showButton = true;
        }
        else
        {
            showButton = false;
        }

        Turn();
    }

    IEnumerator SetupBattle()
    {
        GameObject playerGO = player;
        playerCharacter = playerGO.GetComponent<Personagens>();
        GameObject enemyGO = enemy;
        enemyCharacter = enemyGO.GetComponent<Personagens>();

        dialogueText.text = "TEMOS QUE CORRER PARA QUE ELAS N�O CHEGUEM NA PR�XIMA FASE";

        yield return new WaitForSeconds(5f);

        state = BattleState.PLAYERTURN;
        PlayerTurn();
    }

    void PlayerTurn()
    {
        dialogueText.text = "SEU TURNO";

        if (playerCharacter.atualHp < playerCharacter.maxHp)
        {
            showButtonCure = true;
        }
        else
        {
            showButtonCure = false;
        }
    }

    public void SelecionarInimigo()
    {
        if (state == BattleState.PLAYERTURN)
        {
            selectedEnemy = true;
            showButton = true;
            Som[0].Play();
        }
    }

    public void Toque()
    {
        showButton = false;
        if (state != BattleState.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack());
    }

    public void ToqueCura()
    {
        if (state != BattleState.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal());
    }

    IEnumerator PlayerHeal()
    {
        selectedEnemy = false;
        showButton = false;
        showButtonCure = false;
        playerCharacter.Cura(10);
        Som[2].Play();
        playerHUD.SetHP(playerCharacter.atualHp);

        yield return new WaitForSeconds(2f);

        state = BattleState.ENEMYTURN;
        StartCoroutine(EnemyTurn());
    }

    IEnumerator PlayerAttack()
    {
        selectedEnemy = false;
        showButton = false;
        showButtonCure = false;
        Som[1].Play();
        bool isDead = enemyCharacter.Dano(playerCharacter.dano);
        //enemyHud.SetHP(enemyCharacter.currentHp);
        yield return new WaitForSeconds(3f);

        if (isDead)
        {
            state = BattleState.WON;
            StartCoroutine(EndBattle());
        }
        else
        {
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }

    IEnumerator EnemyTurn()
    {
        Som[3].Play();
        dialogueText.text = "TURNO DO INIMIGO";
        bool isDead = playerCharacter.Dano(enemyCharacter.dano);
        //playerHud.SetHP(playerCharacter.currentHp);
        yield return new WaitForSeconds(2f);

        if (isDead)
        {
            state = BattleState.LOST;
            StartCoroutine(EndBattle());
        }
        else
        {
            state = BattleState.PLAYERTURN;
            PlayerTurn();
            showButton = true;
        }
    }

    public void Turn()
    {
        if(state == BattleState.PLAYERTURN)
        {
            tP1.SetActive(true);
        }
        else
        {
            tP1.SetActive(false); 
        }
    }

    IEnumerator EndBattle()
    {
        if (state == BattleState.WON)
        {
            dialogueText.text = "VOC� VENCEU, PARAB�NS !!!";
            yield return new WaitForSeconds(2f);
            if (FasesManager.fase == 2)
            {
                FasesManager.fase = 3;
                PlayerPrefs.SetInt("fase", FasesManager.fase);
            }
            telaWon.SetActive(true);
            //SceneManager.LoadScene("SelecaoFases");
        }
        else if (state == BattleState.LOST)
        {
            dialogueText.text = "VOC� PERDEU, TENTE NOVAMENTE";
            yield return new WaitForSeconds(2f);
            telaLost.SetActive(true);
            //SceneManager.LoadScene("SelecaoFases");
        }
    }
}
