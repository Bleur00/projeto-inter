using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleStateBoss1 { START, PLAYERTURN, BOSSTURN, PLAYER2TURN, WON, LOST }

public class SistemadeBatalhaBoss1 : MonoBehaviour
{
    public BattleStateBoss1 state;

    [Header("Personagens")]
    public GameObject player;
    public GameObject player2;
    public GameObject boss;

    [Header("Bot�es")]
    public GameObject buttonAttack;
    private bool showButtonAttack;
    public GameObject buttonAttack2;
    private bool showButtonAttack2;
    public GameObject buttonCure;
    private bool showButtonCure;
    public GameObject buttonCure2;
    private bool showButtonCure2;
    public GameObject sB;

    [Header("HUD")]
    [SerializeField]
    CharHUD playerHUD;
    [SerializeField]
    CharHUD player2HUD;
    [SerializeField]
    CharHUD bossHUD;
    public Text dialogueText;
    public GameObject imageSB;
    public GameObject imageSB2;
    public GameObject tP1;
    public GameObject tP2;

    [Header("Telas")]
    public GameObject telaLost;
    public GameObject telaWon;

    [Header("Sons")]
    public AudioSource[] Som;

    Personagens playerCharacter;
    Personagens playerCharacter2;
    Personagens bossCharacter;

    private bool deadP1;
    private bool deadP2;
    private bool deadBoss;

    public int randomCharacter;
    public int attackPlayer;

    public bool selectedBoss;

    public bool p1atk;
    public bool p2atk;

    // Start is called before the first frame update
    void Start()
    {
        state = BattleStateBoss1.START;
        StartCoroutine(SetupBattle());

        deadP1 = false;
        deadP2 = false;
        deadBoss = false;
        showButtonAttack = false;
        showButtonAttack2 = false;
        showButtonCure = false;
        showButtonCure2 = false;
        selectedBoss = false;
        p1atk = false;
        p2atk = false;
        telaWon.SetActive(false);
        telaLost.SetActive(false);

        attackPlayer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        playerHUD.SetHUD(playerCharacter);
        player2HUD.SetHUD(playerCharacter2);
        bossHUD.SetHUD(bossCharacter);

        Dead();

        if (showButtonAttack)
        {
            buttonAttack.SetActive(true);
        }
        else
        {
            buttonAttack.SetActive(false);
        }

        if (showButtonAttack2)
        {
            buttonAttack2.SetActive(true);
        }
        else
        {
            buttonAttack2.SetActive(false);
        }

        if (showButtonCure)
        {
            buttonCure.SetActive(true);
        }
        else
        {
            buttonCure.SetActive(false);
        }

        if (showButtonCure2)
        {
            buttonCure2.SetActive(true);
        }
        else
        {
            buttonCure2.SetActive(false);
        }

        if (state == BattleStateBoss1.PLAYERTURN && selectedBoss)
        {
            imageSB.SetActive(true);
        }
        else
        {
            imageSB.SetActive(false);
        }

        if (state == BattleStateBoss1.PLAYER2TURN && selectedBoss)
        {
            imageSB2.SetActive(true);
        }
        else
        {
            imageSB2.SetActive(false);
        }


        if (playerCharacter.atualHp == playerCharacter2.atualHp)
        {
            attackPlayer = 0;
        }
        else if (playerCharacter.atualHp < playerCharacter2.atualHp)
        {
            attackPlayer = 1;
        }
        else if (playerCharacter.atualHp > playerCharacter2.atualHp)
        {
            attackPlayer = 2;
        }

        if (playerCharacter.atualHp <= 0)
        {
            attackPlayer = 3;
        }
        if (playerCharacter2.atualHp <= 0)
        {
            attackPlayer = 4;
        }

        Turn();

    }

    IEnumerator SetupBattle()
    {
        GameObject playerGO = player;
        playerCharacter = playerGO.GetComponent<Personagens>();
        GameObject player2GO = player2;
        playerCharacter2 = player2GO.GetComponent<Personagens>();
        GameObject bossGO = boss;
        bossCharacter = bossGO.GetComponent<Personagens>();

        dialogueText.text = "V� ESTA CRIATURA QUE N�O PARECE NADA COM UMA C�LULA? BOM, ELA � UMA C�LULA.MAS J� EST� NO EST�GIO DE PROGRESS�O.";

        yield return new WaitForSeconds(5f);

        state = BattleStateBoss1.PLAYERTURN;
        PlayerTurn();
    }

    void PlayerTurn()
    {
        dialogueText.text = "SEU TURNO";

        if (state == BattleStateBoss1.PLAYERTURN && playerCharacter.atualHp < playerCharacter.maxHp)
        {
            showButtonCure = true;
        }
        else
        {
            showButtonCure = false;
        }
    }

    void Player2Turn()
    {
        dialogueText.text = "SEU TURNO";

        if (state == BattleStateBoss1.PLAYER2TURN && playerCharacter2.atualHp < playerCharacter2.maxHp)
        {
            showButtonCure2 = true;
        }
        else
        {
            showButtonCure2 = false;
        }
    }
    public void SelecionarInimigo()
    {
        if (state == BattleStateBoss1.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if(state == BattleStateBoss1.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        Som[0].Play();
        dialogueText.text = "ATAQUE";
        selectedBoss = true;
    }


    public void Toque()
    {
        if (state != BattleStateBoss1.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack());
    }

    public void Toque2()
    {
        if (state != BattleStateBoss1.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack2());
    }

    public void ToqueCura()
    {
        if (state != BattleStateBoss1.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal());
    }

    public void ToqueCura2()
    {
        if (state != BattleStateBoss1.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal2());
    }

    IEnumerator PlayerHeal()
    {
        selectedBoss = false;
        showButtonAttack = false;
        showButtonCure = false;
        p1atk = true;
        p2atk = false;
        playerCharacter.Cura(9);
        Som[2].Play();
        playerHUD.SetHP(playerCharacter.atualHp);

        yield return new WaitForSeconds(2f);

        state = BattleStateBoss1.BOSSTURN;
        StartCoroutine(BossTurn());
    }

    IEnumerator PlayerHeal2()
    {
        selectedBoss = false;
        showButtonAttack2 = false;
        showButtonCure2 = false;
        p1atk = false;
        p2atk = true;
        playerCharacter2.Cura(9);
        Som[2].Play();
        player2HUD.SetHP(playerCharacter2.atualHp);

        yield return new WaitForSeconds(2f);

        state = BattleStateBoss1.BOSSTURN;
        StartCoroutine(BossTurn());
    }

    IEnumerator PlayerAttack()
    {
        selectedBoss = false;
        showButtonAttack = false;
        showButtonCure = false;
        p1atk = true;
        p2atk = false;
        Som[1].Play();
        yield return new WaitForSeconds(2.5f);
        bool isDead = bossCharacter.Dano(playerCharacter.dano);

        if (isDead)
        {
            state = BattleStateBoss1.WON;
            StartCoroutine(EndBattle());
        }
        else
        {
            state = BattleStateBoss1.BOSSTURN;
            StartCoroutine(BossTurn());
        }
    }

    IEnumerator PlayerAttack2()
    {
        selectedBoss = false;
        showButtonAttack2 = false;
        showButtonCure2 = false;
        p1atk = false;
        p2atk = true;
        Som[1].Play();
        yield return new WaitForSeconds(2.5f);
        bool isDead = bossCharacter.Dano(playerCharacter2.dano);

        if (isDead)
        {
            state = BattleStateBoss1.WON;
            StartCoroutine(EndBattle());
        }
        else
        {
            state = BattleStateBoss1.BOSSTURN;
            StartCoroutine(BossTurn());
        }
    }

    IEnumerator BossTurn()
    {
        dialogueText.text = "TURNO DO INIMIGO";
        if (attackPlayer == 0)
        {
            randomCharacter = Random.Range(0, 6);
        }
        else if (attackPlayer == 1)
        {
            randomCharacter = Random.Range(0, 5);
        }
        else if (attackPlayer == 2)
        {
            randomCharacter = Random.Range(1, 6);
        }
        else if (attackPlayer == 3)
        {
            randomCharacter = Random.Range(3, 6);
        }
        else if (attackPlayer == 4)
        {
            randomCharacter = Random.Range(0, 3);
        }
        Som[3].Play();
        yield return new WaitForSeconds(1f);

        if (randomCharacter <= 2)
        {
            bool isDead = playerCharacter.Dano(bossCharacter.dano);

            if (isDead)
            {
                deadP1 = true;
                if (!deadBoss && deadP1 && !deadP2)
                {
                    state = BattleStateBoss1.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadBoss && deadP1 && deadP2)
                {
                    state = BattleStateBoss1.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else if (p1atk)
            {
                if (!deadP2)
                {
                    state = BattleStateBoss1.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP2)
                {
                    state = BattleStateBoss1.PLAYERTURN;
                    PlayerTurn();
                }
            }
            else if (p2atk)
            {
                state = BattleStateBoss1.PLAYERTURN;
                PlayerTurn();
            }
        }
        else if (randomCharacter >= 3)
        {
            bool isDead = playerCharacter2.Dano(bossCharacter.dano);

            if (isDead)
            {
                deadP2 = true;
                if (!deadBoss && !deadP1 && deadP2)
                {
                    state = BattleStateBoss1.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadBoss && deadP1 && deadP2)
                {
                    state = BattleStateBoss1.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else if (p2atk)
            {
                if (!deadP1)
                {
                    state = BattleStateBoss1.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP1)
                {
                    state = BattleStateBoss1.PLAYER2TURN;
                    Player2Turn();
                }
            }
            else if (p1atk)
            {
                state = BattleStateBoss1.PLAYER2TURN;
                Player2Turn();
            }
        }
    }

    public void Dead()
    {
        if (deadP1 && deadP2)
        {
            state = BattleStateBoss1.LOST;
            StartCoroutine(EndBattle());
        }

        if (deadBoss)
        {
            state = BattleStateBoss1.WON;
            StartCoroutine(EndBattle());
        }
    }

    public void Turn()
    {
        if (state == BattleStateBoss1.PLAYERTURN)
        {
            tP1.SetActive(true);
        }
        else
        {
            tP1.SetActive(false);
        }

        if (state == BattleStateBoss1.PLAYER2TURN)
        {
            tP2.SetActive(true);
        }
        else
        {
            tP2.SetActive(false);
        }
    }

    IEnumerator EndBattle()
    {
        if (state == BattleStateBoss1.WON)
        {
            dialogueText.text = "VOC� VENCEU, PARAB�NS !!!";
            yield return new WaitForSeconds(2f);
            if (FasesManager.fase == 4)
            {
                FasesManager.fase = 5;
                PlayerPrefs.SetInt("fase", FasesManager.fase);
            }
            telaWon.SetActive(true);
        }
        else if (state == BattleStateBoss1.LOST)
        {
            dialogueText.text = "VOC� PERDEU, TENTE NOVAMENTE";
            yield return new WaitForSeconds(2f);
            telaLost.SetActive(true);
        }
    }
}
