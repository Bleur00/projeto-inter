using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Personagens : MonoBehaviour
{
    [Header("Status")]
    public int dano;
    public int maxHp;
    public int atualHp;

    public static bool canCure;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(atualHp == maxHp)
        {
            canCure = false;
        }
        else if(atualHp < maxHp)
        {
            canCure = true;
        }
    }

    public bool Dano(int dn)
    {
        atualHp -= dn;

        if (atualHp <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void Cura(int amount)
    {
        atualHp += amount;

        if(atualHp > maxHp)
        {
            atualHp = maxHp;
        }
    }
}
