using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Roteiro : MonoBehaviour
{
    [Header("Sons")]
    public AudioSource[] Som;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Voltar()
    {
        Som[0].Play();
        SceneManager.LoadScene("SelecaoFases");
    }

    public void Continuar()
    {
        Som[0].Play();
        SceneManager.LoadScene("Tutorial");
    }

    public void Continuar1()
    {
        Som[0].Play();
        SceneManager.LoadScene("1x1");
    }

    public void Continuar2()
    {
        Som[0].Play();
        SceneManager.LoadScene("2x2");
    }

    public void Continuar3()
    {
        Som[0].Play();
        SceneManager.LoadScene("BossFase1");
    }

    public void Continuar4()
    {
        Som[0].Play();
        SceneManager.LoadScene("BossFase2");
    }

    public void Continuar5()
    {
        Som[0].Play();
        SceneManager.LoadScene("3x34");
    }

    public void Continuar6()
    {
        Som[0].Play();
        SceneManager.LoadScene("3x37");
    }

    public void Continuar7()
    {
        Som[0].Play();
        SceneManager.LoadScene("Fim");
    }

    public void Fim()
    {
        SceneManager.LoadScene("Menu");
    }
}
