using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TelaLost : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RestartT()
    {
        SceneManager.LoadScene("Tutorial");
    }
    public void RestartF1()
    {
        SceneManager.LoadScene("1x1");
    }
    public void RestartF2()
    {
        SceneManager.LoadScene("2x2");
    }
    public void RestartF3()
    {
        SceneManager.LoadScene("BossFase1");
    }
    public void RestartF4()
    {
        SceneManager.LoadScene("2x2Fase2");
    }
    public void RestartF5()
    {
        SceneManager.LoadScene("3x3");
    }
    public void RestartF6()
    {
        SceneManager.LoadScene("3x32");
    }
    public void RestartF7()
    {
        SceneManager.LoadScene("3x33");
    }
    public void RestartF8()
    {
        SceneManager.LoadScene("BossFase2");
    }
    public void RestartF9()
    {
        SceneManager.LoadScene("3x34");
    }
    public void RestartF10()
    {
        SceneManager.LoadScene("3x35");
    }
    public void RestartF11()
    {
        SceneManager.LoadScene("3x36");
    }
    public void RestartF12()
    {
        SceneManager.LoadScene("3x37");
    }
    public void RestartF13()
    {
        SceneManager.LoadScene("BossFase3");
    }
}
