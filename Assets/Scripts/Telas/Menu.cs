using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [Header("Sons")]
    public AudioSource[] Som;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MenuManager()
    {
        Som[0].Play();
        SceneManager.LoadScene("SelecaoFases");
    }

    public void MenuManager2()
    {
        Som[0].Play();
        Application.Quit();
    }

    public void MenuManager3()
    {
        Som[0].Play();
        SceneManager.LoadScene("SelecionarModo");
    }
}
