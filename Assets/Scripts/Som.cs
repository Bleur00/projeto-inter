using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Som : MonoBehaviour
{
    private bool isMuted;
    // Start is called before the first frame update
    void Start()
    {
        isMuted = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (isMuted)
        {
            AudioAnim.sound = false;
        }
        else
        {
            AudioAnim.sound = true;
        }
    }

    public void Mute()
    {
        isMuted = !isMuted;
        AudioListener.pause = isMuted;
    }
}
