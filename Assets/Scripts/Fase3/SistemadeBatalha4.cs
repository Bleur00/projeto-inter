using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleState4 { START, PLAYERTURN, ENEMYTURN, PLAYER2TURN, ENEMY2TURN, PLAYER3TURN, ENEMY3TURN, PLAYER4TURN, ENEMY4TURN, WON, LOST }
public class SistemadeBatalha4 : MonoBehaviour
{
    public BattleState4 state;

    [Header("Personagens")]
    public GameObject player;
    public GameObject player2;
    public GameObject player3;
    public GameObject player4;
    public GameObject enemy;
    public GameObject enemy2;
    public GameObject enemy3;
    public GameObject enemy4;

    [Header("Bot�es")]
    public GameObject buttonAttack;
    private bool showButtonAttack;
    public GameObject buttonAttack2;
    private bool showButtonAttack2;
    public GameObject buttonAttack3;
    private bool showButtonAttack3;
    public GameObject buttonAttack4;
    private bool showButtonAttack4;
    public GameObject buttonCure;
    private bool showButtonCure;
    public GameObject buttonCure2;
    private bool showButtonCure2;
    public GameObject buttonCure3;
    private bool showButtonCure3;
    public GameObject buttonCure4;
    private bool showButtonCure4;
    public GameObject sE1;
    public GameObject sE2;
    public GameObject sE3;
    public GameObject sE4;

    [Header("HUD")]
    [SerializeField]
    CharHUD playerHUD;
    [SerializeField]
    CharHUD enemyHUD;
    [SerializeField]
    CharHUD player2HUD;
    [SerializeField]
    CharHUD enemy2HUD;
    [SerializeField]
    CharHUD player3HUD;
    [SerializeField]
    CharHUD enemy3HUD;
    [SerializeField]
    CharHUD player4HUD;
    [SerializeField]
    CharHUD enemy4HUD;
    public Text dialogueText;
    public GameObject imageSE1P1;
    public GameObject imageSE2P1;
    public GameObject imageSE3P1;
    public GameObject imageSE4P1;
    public GameObject imageSE1P2;
    public GameObject imageSE2P2;
    public GameObject imageSE3P2;
    public GameObject imageSE4P2;
    public GameObject imageSE1P3;
    public GameObject imageSE2P3;
    public GameObject imageSE3P3;
    public GameObject imageSE4P3;
    public GameObject imageSE1P4;
    public GameObject imageSE2P4;
    public GameObject imageSE3P4;
    public GameObject imageSE4P4;

    Personagens playerCharacter;
    Personagens playerCharacter2;
    Personagens playerCharacter3;
    Personagens playerCharacter4;
    Personagens enemyCharacter;
    Personagens enemyCharacter2;
    Personagens enemyCharacter3;
    Personagens enemyCharacter4;

    private bool deadP1;
    private bool deadP2;
    private bool deadP3;
    private bool deadP4;
    private bool deadE1;
    private bool deadE2;
    private bool deadE3;
    private bool deadE4;

    public int randomCharacter;
    public int attackPlayer;

    public bool selectedEnemy1;
    public bool selectedEnemy2;
    public bool selectedEnemy3;
    public bool selectedEnemy4;
    // Start is called before the first frame update
    void Start()
    {
        state = BattleState4.START;
        StartCoroutine(SetupBattle());

        deadP1 = false;
        deadP2 = false;
        deadP3 = false;
        deadP4 = false;
        deadE1 = false;
        deadE2 = false;
        deadE3 = false;
        deadE4 = false;
        showButtonAttack = false;
        showButtonAttack2 = false;
        showButtonAttack3 = false;
        showButtonAttack4 = false;
        showButtonCure = false;
        showButtonCure2 = false;
        showButtonCure3 = false;
        showButtonCure4 = false;
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = false;

        attackPlayer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        playerHUD.SetHUD(playerCharacter);
        player2HUD.SetHUD(playerCharacter2);
        player3HUD.SetHUD(playerCharacter3);
        player4HUD.SetHUD(playerCharacter4);
        enemyHUD.SetHUD(enemyCharacter);
        enemy2HUD.SetHUD(enemyCharacter2);
        enemy3HUD.SetHUD(enemyCharacter3);
        enemy4HUD.SetHUD(enemyCharacter4);

        Dead();

        if (showButtonAttack)
        {
            buttonAttack.SetActive(true);
        }
        else
        {
            buttonAttack.SetActive(false);
        }

        if (showButtonAttack2)
        {
            buttonAttack2.SetActive(true);
        }
        else
        {
            buttonAttack2.SetActive(false);
        }

        if (showButtonAttack3)
        {
            buttonAttack3.SetActive(true);
        }
        else
        {
            buttonAttack3.SetActive(false);
        }

        if (showButtonAttack4)
        {
            buttonAttack4.SetActive(true);
        }
        else
        {
            buttonAttack4.SetActive(false);
        }

        if (showButtonCure)
        {
            buttonCure.SetActive(true);
        }
        else
        {
            buttonCure.SetActive(false);
        }

        if (showButtonCure2)
        {
            buttonCure2.SetActive(true);
        }
        else
        {
            buttonCure2.SetActive(false);
        }

        if (showButtonCure3)
        {
            buttonCure3.SetActive(true);
        }
        else
        {
            buttonCure3.SetActive(false);
        }

        if (showButtonCure4)
        {
            buttonCure4.SetActive(true);
        }
        else
        {
            buttonCure4.SetActive(false);
        }

        if (playerCharacter.atualHp <= 0)
        {
            attackPlayer = 1;
        }
        if (playerCharacter2.atualHp <= 0)
        {
            attackPlayer = 2;
        }
        if (playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 3;
        }
        if (playerCharacter4.atualHp <= 0)
        {
            attackPlayer = 4;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter2.atualHp <= 0)
        {
            attackPlayer = 5;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 6;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter4.atualHp <= 0)
        {
            attackPlayer = 7;
        }
        if (playerCharacter2.atualHp <= 0 && playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 8;
        }
        if (playerCharacter2.atualHp <= 0 && playerCharacter4.atualHp <= 0)
        {
            attackPlayer = 9;
        }
        if (playerCharacter3.atualHp <= 0 && playerCharacter4.atualHp <= 0)
        {
            attackPlayer = 10;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter2.atualHp <= 0 && playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 11;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter3.atualHp <= 0 && playerCharacter4.atualHp <= 0)
        {
            attackPlayer = 12;
        }
        if (playerCharacter2.atualHp <= 0 && playerCharacter3.atualHp <= 0 && playerCharacter4.atualHp <= 0)
        {
            attackPlayer = 13;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter2.atualHp <= 0 && playerCharacter4.atualHp <= 0)
        {
            attackPlayer = 14;
        }

        if (state == BattleState4.PLAYERTURN && selectedEnemy1)
        {
            imageSE1P1.SetActive(true);
        }
        else
        {
            imageSE1P1.SetActive(false);
        }
        if (state == BattleState4.PLAYERTURN && selectedEnemy2)
        {
            imageSE2P1.SetActive(true);
        }
        else
        {
            imageSE2P1.SetActive(false);
        }
        if (state == BattleState4.PLAYERTURN && selectedEnemy3)
        {
            imageSE3P1.SetActive(true);
        }
        else
        {
            imageSE3P1.SetActive(false);
        }
        if (state == BattleState4.PLAYERTURN && selectedEnemy4)
        {
            imageSE4P1.SetActive(true);
        }
        else
        {
            imageSE4P1.SetActive(false);
        }
        if (state == BattleState4.PLAYER2TURN && selectedEnemy1)
        {
            imageSE1P2.SetActive(true);
        }
        else
        {
            imageSE1P2.SetActive(false);
        }
        if (state == BattleState4.PLAYER2TURN && selectedEnemy2)
        {
            imageSE2P2.SetActive(true);
        }
        else
        {
            imageSE2P2.SetActive(false);
        }
        if (state == BattleState4.PLAYER2TURN && selectedEnemy3)
        {
            imageSE3P2.SetActive(true);
        }
        else
        {
            imageSE3P2.SetActive(false);
        }
        if (state == BattleState4.PLAYER2TURN && selectedEnemy4)
        {
            imageSE4P2.SetActive(true);
        }
        else
        {
            imageSE4P2.SetActive(false);
        }
        if (state == BattleState4.PLAYER3TURN && selectedEnemy1)
        {
            imageSE1P3.SetActive(true);
        }
        else
        {
            imageSE1P3.SetActive(false);
        }
        if (state == BattleState4.PLAYER3TURN && selectedEnemy2)
        {
            imageSE2P3.SetActive(true);
        }
        else
        {
            imageSE2P3.SetActive(false);
        }
        if (state == BattleState4.PLAYER3TURN && selectedEnemy3)
        {
            imageSE3P3.SetActive(true);
        }
        else
        {
            imageSE3P3.SetActive(false);
        }
        if (state == BattleState4.PLAYER3TURN && selectedEnemy4)
        {
            imageSE4P3.SetActive(true);
        }
        else
        {
            imageSE4P3.SetActive(false);
        }
        if (state == BattleState4.PLAYER4TURN && selectedEnemy1)
        {
            imageSE1P4.SetActive(true);
        }
        else
        {
            imageSE1P4.SetActive(false);
        }
        if (state == BattleState4.PLAYER4TURN && selectedEnemy2)
        {
            imageSE2P4.SetActive(true);
        }
        else
        {
            imageSE2P4.SetActive(false);
        }
        if (state == BattleState4.PLAYER4TURN && selectedEnemy3)
        {
            imageSE3P4.SetActive(true);
        }
        else
        {
            imageSE3P4.SetActive(false);
        }
        if (state == BattleState4.PLAYER4TURN && selectedEnemy4)
        {
            imageSE4P4.SetActive(true);
        }
        else
        {
            imageSE4P4.SetActive(false);
        }

        if (deadE1)
        {
            selectedEnemy1 = false;
        }
        if (deadE2)
        {
            selectedEnemy2 = false;
        }
        if (deadE3)
        {
            selectedEnemy3 = false;
        }
        if (deadE4)
        {
            selectedEnemy4 = false;
        }
    }

    IEnumerator SetupBattle()
    {
        GameObject playerGO = player;
        playerCharacter = playerGO.GetComponent<Personagens>();
        GameObject player2GO = player2;
        playerCharacter2 = player2GO.GetComponent<Personagens>();
        GameObject player3GO = player3;
        playerCharacter3 = player3GO.GetComponent<Personagens>();
        GameObject player4GO = player4;
        playerCharacter4 = player4GO.GetComponent<Personagens>();
        GameObject enemyGO = enemy;
        enemyCharacter = enemyGO.GetComponent<Personagens>();
        GameObject enemy2GO = enemy2;
        enemyCharacter2 = enemy2GO.GetComponent<Personagens>();
        GameObject enemy3GO = enemy3;
        enemyCharacter3 = enemy3GO.GetComponent<Personagens>();
        GameObject enemy4GO = enemy4;
        enemyCharacter4 = enemy4GO.GetComponent<Personagens>();

        dialogueText.text = "Start";

        yield return new WaitForSeconds(2f);

        state = BattleState4.PLAYERTURN;
        PlayerTurn();
    }

    void PlayerTurn()
    {
        dialogueText.text = "Seu Turno";

        if (playerCharacter.atualHp < playerCharacter.maxHp)
        {
            showButtonCure = true;
        }
        else
        {
            showButtonCure = false;
        }
    }

    void Player2Turn()
    {
        dialogueText.text = "Seu turno, Escolha Um Inimigo !!!";

        if (playerCharacter2.atualHp < playerCharacter2.maxHp)
        {
            showButtonCure2 = true;
        }
        else
        {
            showButtonCure2 = false;
        }
    }

    void Player3Turn()
    {
        dialogueText.text = "Seu turno, Escolha Um Inimigo !!!";

        if (playerCharacter3.atualHp < playerCharacter3.maxHp)
        {
            showButtonCure3 = true;
        }
        else
        {
            showButtonCure3 = false;
        }
    }

    void Player4Turn()
    {
        dialogueText.text = "Seu turno, Escolha Um Inimigo !!!";

        if (playerCharacter4.atualHp < playerCharacter4.maxHp)
        {
            showButtonCure4 = true;
        }
        else
        {
            showButtonCure4 = false;
        }
    }

    public void SelecionarInimigo1()
    {
        if (state == BattleState4.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if (state == BattleState4.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        else if (state == BattleState4.PLAYER3TURN)
        {
            showButtonAttack3 = true;
        }
        else if (state == BattleState4.PLAYER4TURN)
        {
            showButtonAttack4 = true;
        }

        dialogueText.text = "Ataque";
        selectedEnemy1 = true;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = false;
    }

    public void SelecionarInimigo2()
    {
        if (state == BattleState4.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if (state == BattleState4.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        else if (state == BattleState4.PLAYER3TURN)
        {
            showButtonAttack3 = true;
        }
        else if (state == BattleState4.PLAYER4TURN)
        {
            showButtonAttack4 = true;
        }

        dialogueText.text = "Ataque";
        selectedEnemy1 = false;
        selectedEnemy2 = true;
        selectedEnemy3 = false;
        selectedEnemy4 = false;
    }

    public void SelecionarInimigo3()
    {
        if (state == BattleState4.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if (state == BattleState4.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        else if (state == BattleState4.PLAYER3TURN)
        {
            showButtonAttack3 = true;
        }
        else if (state == BattleState4.PLAYER4TURN)
        {
            showButtonAttack4 = true;
        }

        dialogueText.text = "Ataque";
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = true;
        selectedEnemy4 = false;
    }

    public void SelecionarInimigo4()
    {
        if (state == BattleState4.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if (state == BattleState4.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        else if (state == BattleState4.PLAYER3TURN)
        {
            showButtonAttack3 = true;
        }
        else if (state == BattleState4.PLAYER4TURN)
        {
            showButtonAttack4 = true;
        }

        dialogueText.text = "Ataque";
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = true;
    }

    public void Toque()
    {
        if (state != BattleState4.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack());
    }

    public void Toque2()
    {
        if (state != BattleState4.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack2());
    }

    public void Toque3()
    {
        if (state != BattleState4.PLAYER3TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack3());
    }

    public void Toque4()
    {
        if (state != BattleState4.PLAYER4TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack4());
    }

    public void ToqueCura()
    {
        if (state != BattleState4.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal());
    }

    public void ToqueCura2()
    {
        if (state != BattleState4.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal2());
    }

    public void ToqueCura3()
    {
        if (state != BattleState4.PLAYER3TURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal3());
    }

    public void ToqueCura4()
    {
        if (state != BattleState4.PLAYER4TURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal4());
    }

    IEnumerator PlayerHeal()
    {
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = false;
        showButtonAttack = false;
        showButtonCure = false;
        playerCharacter.Cura(8);

        playerHUD.SetHP(playerCharacter.atualHp);

        yield return new WaitForSeconds(2f);

        if (!deadE1)
        {
            state = BattleState4.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
        else if (deadE1 && !deadP2)
        {
            state = BattleState4.PLAYER2TURN;
            Player2Turn();
        }
        else if (deadE1 && deadP2 && !deadE2)
        {
            state = BattleState4.ENEMY2TURN;
            StartCoroutine(Enemy2Turn());
        }
        else if (deadE1 && deadP2 && deadE2 && !deadP3)
        {
            state = BattleState4.PLAYER3TURN;
            Player3Turn();
        }
        else if (deadE1 && deadP2 && deadE2 && deadP3 && !deadE3)
        {
            state = BattleState4.ENEMY3TURN;
            StartCoroutine(Enemy3Turn());
        }
        else if (deadE1 && deadP2 && deadE2 && deadP3 && deadE3 && !deadP4)
        {
            state = BattleState4.PLAYER4TURN;
            Player4Turn();
        }
        else if (deadE1 && deadP2 && deadE2 && deadP3 && deadE3 && deadP4 && !deadE4)
        {
            state = BattleState4.ENEMY4TURN;
            StartCoroutine(Enemy4Turn());
        }
    }

    IEnumerator PlayerHeal2()
    {
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = false;
        showButtonAttack2 = false;
        showButtonCure2 = false;
        playerCharacter2.Cura(8);

        player2HUD.SetHP(playerCharacter2.atualHp);

        yield return new WaitForSeconds(2f);

        if (!deadE2)
        {
            state = BattleState4.ENEMY2TURN;
            StartCoroutine(Enemy2Turn());
        }
        else if (deadE2 && !deadP3)
        {
            state = BattleState4.PLAYER3TURN;
            Player3Turn();
        }
        else if (deadE2 && deadP3 && !deadE3)
        {
            state = BattleState4.ENEMY3TURN;
            StartCoroutine(Enemy3Turn());
        }
        else if (deadE2 && deadP3 && deadE3 && !deadP4)
        {
            state = BattleState4.PLAYER4TURN;
            Player4Turn();
        }
        else if (deadE2 && deadP3 && deadE3 && deadP4 && !deadE4)
        {
            state = BattleState4.ENEMY4TURN;
            StartCoroutine(Enemy4Turn());
        }
        else if (deadE2 && deadP3 && deadE3 && deadP4 && deadE4 && !deadP1)
        {
            state = BattleState4.PLAYERTURN;
            PlayerTurn();
        }
        else if (deadE2 && deadP3 && deadE3 && deadP4 && deadE4 && deadP1 && !deadE1)
        {
            state = BattleState4.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }

    IEnumerator PlayerHeal3()
    {
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = false;
        showButtonAttack3 = false;
        showButtonCure3 = false;
        playerCharacter3.Cura(8);

        player3HUD.SetHP(playerCharacter3.atualHp);

        yield return new WaitForSeconds(2f);

        if (!deadE3)
        {
            state = BattleState4.ENEMY3TURN;
            StartCoroutine(Enemy3Turn());
        }
        else if (deadE3 && !deadP4)
        {
            state = BattleState4.PLAYER4TURN;
            Player4Turn();
        }
        else if (deadE3 && deadP4 && !deadE4)
        {
            state = BattleState4.ENEMY4TURN;
            StartCoroutine(Enemy4Turn());
        }
        else if (deadE3 && deadP4 && deadE4 && !deadP1)
        {
            state = BattleState4.PLAYERTURN;
            PlayerTurn();
        }
        else if (deadE3 && deadP4 && deadE4 && deadP1 && !deadE1)
        {
            state = BattleState4.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
        else if (deadE3 && deadP4 && deadE4 && deadP1 && deadE1 && !deadP2)
        {
            state = BattleState4.PLAYER2TURN;
            Player2Turn();
        }
        else if (deadE3 && deadP4 && deadE4 && deadP1 && deadE1 && deadP2 && !deadE2)
        {
            state = BattleState4.ENEMY2TURN;
            StartCoroutine(Enemy2Turn());
        }
    }

    IEnumerator PlayerHeal4()
    {
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = false;
        showButtonAttack4 = false;
        showButtonCure4 = false;
        playerCharacter4.Cura(8);

        player4HUD.SetHP(playerCharacter4.atualHp);

        yield return new WaitForSeconds(2f);

        if (!deadE4)
        {
            state = BattleState4.ENEMY4TURN;
            StartCoroutine(Enemy4Turn());
        }
        else if (deadE4 && !deadP1)
        {
            state = BattleState4.PLAYERTURN;
            PlayerTurn();
        }
        else if (deadE4 && deadP1 && !deadE1)
        {
            state = BattleState4.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
        else if (deadE4 && deadP1 && deadE1 && !deadP2)
        {
            state = BattleState4.PLAYER2TURN;
            Player2Turn();
        }
        else if (deadE4 && deadP1 && deadE1 && deadP2 && !deadE2)
        {
            state = BattleState4.ENEMY2TURN;
            StartCoroutine(Enemy2Turn());
        }
        else if (deadE4 && deadP1 && deadE1 && deadP2 && deadE2 && !deadP3)
        {
            state = BattleState4.PLAYER3TURN;
            Player3Turn();
        }
        else if (deadE4 && deadP1 && deadE1 && deadP2 && deadE2 && deadP3 && !deadE3)
        {
            state = BattleState4.ENEMY3TURN;
            StartCoroutine(Enemy3Turn());
        }
    }

    IEnumerator PlayerAttack()
    {
        showButtonAttack = false;
        showButtonCure = false;
        yield return new WaitForSeconds(0.1f);

        if (selectedEnemy1)
        {
            bool isDead = enemyCharacter.Dano(playerCharacter.dano);

            if (isDead)
            {
                deadE1 = true;
                if (!deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP2 && deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP2 && deadE2 && deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP2 && deadE2 && deadP3 && deadE3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP2 && deadE2 && deadP3 && deadE3 && deadP4 && deadE4)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
        }
        else if (selectedEnemy2)
        {
            bool isDead = enemyCharacter2.Dano(playerCharacter.dano);

            if (isDead)
            {
                deadE2 = true;
                if (!deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadP2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && deadP2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && deadP2 && deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadE1 && deadP2 && deadP3 && deadE3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE1 && deadP2 && deadP3 && deadE3 && deadP4 && deadE4)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE1 && !deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadE1 && deadP2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
        }
        else if (selectedEnemy3)
        {
            bool isDead = enemyCharacter3.Dano(playerCharacter.dano);

            if (isDead)
            {
                deadE3 = true;
                if (!deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && deadP2 && deadE2 && deadP3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadE1 && deadP2 && deadE2 && deadP3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE1 && deadP2 && deadE2 && deadP3 && deadP4 && deadE4)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE1 && !deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadE1 && deadP2 && !deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadE1 && deadP2 && deadE2 && !deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadE1 && deadP2 && deadE2 && deadP3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
        }

        else if (selectedEnemy4)
        {
            bool isDead = enemyCharacter4.Dano(playerCharacter.dano);

            if (isDead)
            {
                deadE4 = true;
                if (!deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && deadP2 && deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && deadP2 && deadE2 && deadP3 && deadE3)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE1 && !deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadE1 && deadP2 && !deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadE1 && deadP2 && deadE2 && !deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadE1 && deadP2 && deadE2 && deadP3 && !deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadE1 && deadP2 && deadE2 && deadP3 && deadE3 && !deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadE1 && deadP2 && deadE2 && deadP3 && deadE3 && deadP4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
        }

        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = false;
    }

    IEnumerator PlayerAttack2()
    {
        showButtonAttack2 = false;
        showButtonCure2 = false;
        yield return new WaitForSeconds(0.1f);

        if (selectedEnemy1)
        {
            bool isDead = enemyCharacter.Dano(playerCharacter2.dano);

            if (isDead)
            {
                deadE1 = true;
                if (!deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE2 && deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadE2 && deadP3 && deadE3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE2 && deadP3 && deadE3 && deadP4 && deadE4)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadE2 && !deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadE2 && deadP3 && !deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadE2 && deadP3 && deadE3 && !deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadE2 && deadP3 && deadE3 && deadP4 && !deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadE2 && deadP3 && deadE3 && deadP4 && deadE4 && !deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE2 && deadP3 && deadE3 && deadP4 && deadE4 && deadP1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
        }
        else if (selectedEnemy2)
        {
            bool isDead = enemyCharacter2.Dano(playerCharacter2.dano);

            if (isDead)
            {
                deadE2 = true;
                if (!deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP3 && deadE3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP3 && deadE3 && deadP4 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP3 && deadE3 && deadP4 && deadE4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP3 && deadE3 && deadP4 && deadE4 && deadP1 && deadE1)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
        }
        else if (selectedEnemy3)
        {
            bool isDead = enemyCharacter3.Dano(playerCharacter2.dano);

            if (isDead)
            {
                deadE3 = true;
                if (!deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP3 && deadP4 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP3 && deadP4 && deadE4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP3 && deadP4 && deadE4 && deadP1 && deadE1 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP3 && deadP4 && deadE4 && deadP1 && deadE1 && deadE2)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if(!deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
        }

        else if (selectedEnemy4)
        {
            bool isDead = enemyCharacter4.Dano(playerCharacter2.dano);

            if (isDead)
            {
                deadE4 = true;
                if (!deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE2 && deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadE2 && deadP3 && deadE3 && deadP4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE2 && deadP3 && deadE3 && deadP4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE2 && deadP3 && deadE3 && deadP4 && deadP1 && deadE1)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadE2 && !deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadE2 && deadP3 && !deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadE2 && deadP3 && deadE3 && !deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadE2 && deadP3 && deadE3 && deadP4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
        }

        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = false;
    }

    IEnumerator PlayerAttack3()
    {
        showButtonAttack3 = false;
        showButtonCure3 = false;
        yield return new WaitForSeconds(0.1f);

        if (selectedEnemy1)
        {
            bool isDead = enemyCharacter.Dano(playerCharacter3.dano);

            if (isDead)
            {
                deadE1 = true;
                if (!deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadE3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE3 && deadP4 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE3 && deadP4 && deadE4 && deadP1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE3 && deadP4 && deadE4 && deadP1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE3 && deadP4 && deadE4 && deadP1 && deadP2 && deadE2)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadE3 && !deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadE3 && deadP4 && !deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadE3 && deadP4 && deadE4 && !deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE3 && deadP4 && deadE4 && deadP1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
        }
        else if (selectedEnemy2)
        {
            bool isDead = enemyCharacter2.Dano(playerCharacter3.dano);

            if (isDead)
            {
                deadE2 = true;
                if (!deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadE3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE3 && deadP4 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE3 && deadP4 && deadE4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE3 && deadP4 && deadE4 && deadP1 && deadE1)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadE3 && !deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadE3 && deadP4 && !deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadE3 && deadP4 && deadE4 && !deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE3 && deadP4 && deadE4 && deadP1 && !deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE3 && deadP4 && deadE4 && deadP1 && deadE1 && !deadP2)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE3 && deadP4 && deadE4 && deadP1 && deadE1 && deadP2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
        }
        else if (selectedEnemy3)
        {
            bool isDead = enemyCharacter3.Dano(playerCharacter3.dano);

            if (isDead)
            {
                deadE3 = true;
                if (!deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP4 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP4 && deadE4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP4 && deadE4 && deadP1 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP4 && deadE4 && deadP1 && deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP4 && deadE4 && deadP1 && deadE1 && deadP2 && deadE2)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else 
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
        }

        else if (selectedEnemy4)
        {
            bool isDead = enemyCharacter4.Dano(playerCharacter3.dano);

            if (isDead)
            {
                deadE4 = true;
                if (!deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadE3 && deadP4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE3 && deadP4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE3 && deadP4 && deadP1 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE3 && deadP4 && deadP1 && deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE3 && deadP4 && deadP1 && deadE1 && deadP2 && deadE2)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadE3 && !deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadE4 && deadP4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
        }

        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = false;
    }

    IEnumerator PlayerAttack4()
    {
        showButtonAttack4 = false;
        showButtonCure4 = false;
        yield return new WaitForSeconds(0.1f);

        if (selectedEnemy1)
        {
            bool isDead = enemyCharacter.Dano(playerCharacter4.dano);

            if (isDead)
            {
                deadE1 = true;
                if (!deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE4 && deadP1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE4 && deadP1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE4 && deadP1 && deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE4 && deadP1 && deadP2 && deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE4 && deadP1 && deadP2 && deadE2 && deadP3 && deadE3)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadE4 && !deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE4 && deadP1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
        }
        else if (selectedEnemy2)
        {
            bool isDead = enemyCharacter2.Dano(playerCharacter4.dano);

            if (isDead)
            {
                deadE2 = true;
                if (!deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE4 && deadP1 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE4 && deadP1 && deadE1 && deadP2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE4 && deadP1 && deadE1 && deadP2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE4 && deadP1 && deadE1 && deadP2 && deadP3 && deadE3)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadE4 && !deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE4 && deadP1 && !deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE4 && deadP1 && deadE1 && !deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadE4 && deadP1 && deadE1 && deadP2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
        }
        else if (selectedEnemy3)
        {
            bool isDead = enemyCharacter3.Dano(playerCharacter4.dano);

            if (isDead)
            {
                deadE3 = true;
                if (!deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE4 && deadP1 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE4 && deadP1 && deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE4 && deadP1 && deadE1 && deadP2 && deadE2)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadE4 && !deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE4 && deadP1 && !deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE4 && deadP1 && deadE1 && !deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadE4 && deadP1 && deadE1 && deadP2 && !deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadE4 && deadP1 && deadE1 && deadP2 && deadE2 && !deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadE4 && deadP1 && deadE1 && deadP2 && deadE2 && deadP3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
        }

        else if (selectedEnemy4)
        {
            bool isDead = enemyCharacter4.Dano(playerCharacter4.dano);

            if (isDead)
            {
                deadE4 = true;
                if (!deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP1 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP1 && deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP1 && deadE1 && deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP1 && deadE1 && deadP2 && deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP1 && deadE1 && deadP2 && deadE2 && deadP3 && deadE3)
                {
                    state = BattleState4.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
        }

        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        selectedEnemy4 = false;
    }

    IEnumerator EnemyTurn()
    {
        dialogueText.text = "Turno do inimigo";
        if (attackPlayer == 0)
        {
            randomCharacter = Random.Range(1, 5);
        }
        else if (attackPlayer == 1)
        {
            randomCharacter = Random.Range(2, 5);
        }
        else if (attackPlayer == 2)
        {
            randomCharacter = Random.Range(1, 5);
            if (randomCharacter == 2)
            {
                randomCharacter = Random.Range(1, 5);
            }
        }
        else if (attackPlayer == 3)
        {
            randomCharacter = Random.Range(1, 5);
            if (randomCharacter == 3)
            {
                randomCharacter = Random.Range(1, 5);
            }
        }
        else if (attackPlayer == 4)
        {
            randomCharacter = Random.Range(2, 5);
        }
        else if (attackPlayer == 5)
        {
            randomCharacter = Random.Range(3, 5);
        }
        else if (attackPlayer == 6)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 7)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 8)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 9)
        {
            randomCharacter = 1;
        }
        else if (attackPlayer == 10)
        {
            randomCharacter = Random.Range(1, 3);
        }
        else if (attackPlayer == 11)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 12)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 13)
        {
            randomCharacter = 1;
        }
        else if (attackPlayer == 14)
        {
            randomCharacter = 3;
        }

        yield return new WaitForSeconds(1f);

        if (randomCharacter == 1)
        {
            bool isDead = playerCharacter.Dano(enemyCharacter.dano);

            if (isDead)
            {
                deadP1 = true;
                if (!deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP2 && deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP2 && deadE2 && deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP2 && deadE2 && deadP3 && deadE3 && deadP4)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            if (!deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP2 && !deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadP2 && deadE2 && !deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP2 && deadE2 && deadP3 && !deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadP2 && deadE2 && deadP3 && deadE3 && !deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadP2 && deadE2 && deadP3 && deadE3 && deadP4 && !deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadP2 && deadE2 && deadP3 && deadE3 && deadP4 && deadE4)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
        }
        else if (randomCharacter == 2)
        {
            bool isDead = playerCharacter2.Dano(enemyCharacter.dano);

            if (isDead)
            {
                deadP2 = true;
                if (!deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE2 && deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadE2 && deadP3 && deadE3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE2 && deadP3 && deadE3 && deadP4 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE2 && deadP3 && deadE3 && deadP4 && deadE4 && deadP1)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }

            }
            else
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
        }
        else if (randomCharacter == 3)
        {
            bool isDead = playerCharacter3.Dano(enemyCharacter.dano);

            if (isDead)
            {
                deadP3 = true;
                if (!deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP2 && deadE2 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP2 && deadE2 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP2 && deadE2 && deadE3 && deadP4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP2 && deadE2 && deadE3 && deadP4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP2 && deadE2 && deadE3 && deadP4 && deadP1)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            if (!deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP2 && !deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadP2 && deadE2)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }

        }

        else if (randomCharacter == 4)
        {
            bool isDead = playerCharacter4.Dano(enemyCharacter.dano);

            if (isDead)
            {
                deadP4 = true;
                if (!deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP2 && deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP2 && deadE2 && deadP3 && deadE3 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP2 && deadE2 && deadP3 && deadE3 && deadP1)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            if (!deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP2 && !deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadP2 && deadE2 && !deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP2 && deadE2 && deadP3 && !deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadP2 && deadE2 && deadP3 && deadE3)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
        }

    }

    IEnumerator Enemy2Turn()
    {
        dialogueText.text = "Turno do inimigo";
        if (attackPlayer == 0)
        {
            randomCharacter = Random.Range(1, 5);
        }
        else if (attackPlayer == 1)
        {
            randomCharacter = Random.Range(2, 5);
        }
        else if (attackPlayer == 2)
        {
            randomCharacter = Random.Range(1, 5);
            if (randomCharacter == 2)
            {
                randomCharacter = Random.Range(1, 5);
            }
        }
        else if (attackPlayer == 3)
        {
            randomCharacter = Random.Range(1, 5);
            if (randomCharacter == 3)
            {
                randomCharacter = Random.Range(1, 5);
            }
        }
        else if (attackPlayer == 4)
        {
            randomCharacter = Random.Range(2, 5);
        }
        else if (attackPlayer == 5)
        {
            randomCharacter = Random.Range(3, 5);
        }
        else if (attackPlayer == 6)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 7)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 8)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 9)
        {
            randomCharacter = 1;
        }
        else if (attackPlayer == 10)
        {
            randomCharacter = Random.Range(1, 3);
        }
        else if (attackPlayer == 11)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 12)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 13)
        {
            randomCharacter = 1;
        }
        else if (attackPlayer == 14)
        {
            randomCharacter = 3;
        }

        yield return new WaitForSeconds(1f);

        if (randomCharacter == 1)
        {
            bool isDead = playerCharacter.Dano(enemyCharacter2.dano);

            if (isDead)
            {
                deadP1 = true;
                if (!deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP3 && deadE3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP3 && deadE3 && deadP4 && deadE4 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP3 && deadE3 && deadP4 && deadE4 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP3 && deadE3 && deadP4 && deadE4 && deadE1 && deadP2)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP3 && !deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadP3 && deadE3 && !deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadP3 && deadE3 && deadP4 && !deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadP3 && deadE3 && deadP4 && deadE4)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
        }
        else if (randomCharacter == 2)
        {
            bool isDead = playerCharacter2.Dano(enemyCharacter2.dano);

            if (isDead)
            {
                deadP2 = true;
                if (!deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP3 && deadE3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP3 && deadE3 && deadP4 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP3 && deadE3 && deadP4 && deadE4 && deadP1)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP3 && !deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadP3 && deadE3 && !deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadP3 && deadE3 && deadP4 && !deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadP3 && deadE3 && deadP4 && deadE4 && !deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP3 && deadE3 && deadP4 && deadE4 && deadP1 && !deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadP3 && deadE3 && deadP4 && deadE4 && deadP1 && deadE1)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
        }
        else if (randomCharacter == 3)
        {
            bool isDead = playerCharacter3.Dano(enemyCharacter2.dano);

            if (isDead)
            {
                deadP3 = true;
                if (!deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadE3 && deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE3 && deadP4 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE3 && deadP4 && deadE4 && deadP1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE3 && deadP4 && deadE4 && deadP1 && deadP2)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }

        }

        else if (randomCharacter == 4)
        {
            bool isDead = playerCharacter4.Dano(enemyCharacter2.dano);

            if (isDead)
            {
                deadP4 = true;
                if (!deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP3 && deadE3 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP3 && deadE3 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP3 && deadE3 && deadE4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP3 && deadE3 && deadE4 && deadP1 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP3 && deadE3 && deadE4 && deadP1 && deadE1 && deadP2)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP3 && !deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadP3 && deadE3)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }

        }
    }

    IEnumerator Enemy3Turn()
    {
        dialogueText.text = "Turno do inimigo";
        if (attackPlayer == 0)
        {
            randomCharacter = Random.Range(1, 5);
        }
        else if (attackPlayer == 1)
        {
            randomCharacter = Random.Range(2, 5);
        }
        else if (attackPlayer == 2)
        {
            randomCharacter = Random.Range(1, 5);
            if (randomCharacter == 2)
            {
                randomCharacter = Random.Range(1, 5);
            }
        }
        else if (attackPlayer == 3)
        {
            randomCharacter = Random.Range(1, 5);
            if (randomCharacter == 3)
            {
                randomCharacter = Random.Range(1, 5);
            }
        }
        else if (attackPlayer == 4)
        {
            randomCharacter = Random.Range(2, 5);
        }
        else if (attackPlayer == 5)
        {
            randomCharacter = Random.Range(3, 5);
        }
        else if (attackPlayer == 6)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 7)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 8)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 9)
        {
            randomCharacter = 1;
        }
        else if (attackPlayer == 10)
        {
            randomCharacter = Random.Range(1, 3);
        }
        else if (attackPlayer == 11)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 12)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 13)
        {
            randomCharacter = 1;
        }
        else if (attackPlayer == 14)
        {
            randomCharacter = 3;
        }

        yield return new WaitForSeconds(1f);

        if (randomCharacter == 1)
        {
            bool isDead = playerCharacter.Dano(enemyCharacter3.dano);

            if (isDead)
            {
                deadP1 = true;
                if (!deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP4 && deadE4 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP4 && deadE4 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP4 && deadE4 && deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP4 && deadE4 && deadE1 && deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
                else if (deadP4 && deadE4 && deadE1 && deadP2 && deadE2 && deadP3)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            if (!deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadP4 && !deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadP4 && deadE4)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
        }
        else if (randomCharacter == 2)
        {
            bool isDead = playerCharacter2.Dano(enemyCharacter3.dano);

            if (isDead)
            {
                deadP2 = true;
                if (!deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP4 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP4 && deadE4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP4 && deadE4 && deadP1 && deadE1 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP4 && deadE4 && deadP1 && deadE1 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP4 && deadE4 && deadP1 && deadE1 && deadE2 && deadP3)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            if (!deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadP4 && !deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadP4 && deadE4 && !deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP4 && deadE4 && deadP1 && !deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadP4 && deadE4 && deadP1 && deadE1)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
        }
        else if (randomCharacter == 3)
        {
            bool isDead = playerCharacter3.Dano(enemyCharacter3.dano);

            if (isDead)
            {
                deadP3 = true;
                if (!deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP4 && !deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadP4 && deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP4 && deadE4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP4 && deadE4 && deadP1 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP4 && deadE4 && deadP1 && deadE1 && deadP2)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            if (!deadP4)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }
            else if (deadP4 && !deadE4)
            {
                state = BattleState4.ENEMY4TURN;
                StartCoroutine(Enemy4Turn());
            }
            else if (deadP4 && deadE4 && !deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP4 && deadE4 && deadP1 && !deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadP4 && deadE4 && deadP1 && deadE1 && !deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP4 && deadE4 && deadP1 && deadE1 && deadP2 && !deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadP4 && deadE4 && deadP1 && deadE1 && deadP2 && deadE2)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }

        }

        else if (randomCharacter == 4)
        {
            bool isDead = playerCharacter4.Dano(enemyCharacter3.dano);

            if (isDead)
            {
                deadP4 = true;
                if (!deadE4)
                {
                    state = BattleState4.ENEMY4TURN;
                    StartCoroutine(Enemy4Turn());
                }
                else if (deadE4 && !deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE4 && deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE4 && deadP1 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE4 && deadP1 && deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE4 && deadP1 && deadE1 && deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE4 && deadP1 && deadE1 && deadP2 && deadE2 && deadP3)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }

        }
    }

    IEnumerator Enemy4Turn()
    {
        dialogueText.text = "Turno do inimigo";
        if (attackPlayer == 0)
        {
            randomCharacter = Random.Range(1, 5);
        }
        else if (attackPlayer == 1)
        {
            randomCharacter = Random.Range(2, 5);
        }
        else if (attackPlayer == 2)
        {
            randomCharacter = Random.Range(1, 5);
            if (randomCharacter == 2)
            {
                randomCharacter = Random.Range(1, 5);
            }
        }
        else if (attackPlayer == 3)
        {
            randomCharacter = Random.Range(1, 5);
            if (randomCharacter == 3)
            {
                randomCharacter = Random.Range(1, 5);
            }
        }
        else if (attackPlayer == 4)
        {
            randomCharacter = Random.Range(2, 5);
        }
        else if (attackPlayer == 5)
        {
            randomCharacter = Random.Range(3, 5);
        }
        else if (attackPlayer == 6)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 7)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 8)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 9)
        {
            randomCharacter = 1;
        }
        else if (attackPlayer == 10)
        {
            randomCharacter = Random.Range(1, 3);
        }
        else if (attackPlayer == 11)
        {
            randomCharacter = 4;
        }
        else if (attackPlayer == 12)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 13)
        {
            randomCharacter = 1;
        }
        else if (attackPlayer == 14)
        {
            randomCharacter = 3;
        }

        yield return new WaitForSeconds(1f);

        if (randomCharacter == 1)
        {
            bool isDead = playerCharacter.Dano(enemyCharacter4.dano);

            if (isDead)
            {
                deadP1 = true;
                if (!deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && deadP2 && deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && deadP2 && deadE2 && deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadE1 && deadP2 && deadE2 && deadP3 && deadE3 && deadP4)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
        }
        else if (randomCharacter == 2)
        {
            bool isDead = playerCharacter2.Dano(enemyCharacter4.dano);

            if (isDead)
            {
                deadP2 = true;
                if (!deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP1 && deadE1 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP1 && deadE1 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP1 && deadE1 && deadE2 && deadP3 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP1 && deadE1 && deadE2 && deadP3 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP1 && deadE1 && deadE2 && deadP3 && deadE3 && deadP4)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            if (!deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP1 && !deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadP1 && deadE1)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
        }
        else if (randomCharacter == 3)
        {
            bool isDead = playerCharacter3.Dano(enemyCharacter4.dano);

            if (isDead)
            {
                deadP3 = true;
                if (!deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP1 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP1 && deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP1 && deadE1 && deadP2 && deadE2 && !deadE3)
                {
                    state = BattleState4.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadP1 && deadE1 && deadP2 && deadE2 && deadE3 && !deadP4)
                {
                    state = BattleState4.PLAYER4TURN;
                    Player4Turn();
                }
                else if (deadP1 && deadE1 && deadP2 && deadE2 && deadE3 && deadP4)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            if (!deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP1 && !deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadP1 && deadE1 && !deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP1 && deadE1 && deadP2 && !deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadP1 && deadE1 && deadP2 && deadE2)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }

        }

        else if (randomCharacter == 4)
        {
            bool isDead = playerCharacter4.Dano(enemyCharacter4.dano);

            if (isDead)
            {
                deadP4 = true;
                if (!deadP1)
                {
                    state = BattleState4.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP1 && !deadE1)
                {
                    state = BattleState4.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadP1 && deadE1 && !deadP2)
                {
                    state = BattleState4.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP1 && deadE1 && deadP2 && !deadE2)
                {
                    state = BattleState4.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadP1 && deadE1 && deadP2 && deadE2 && !deadP3)
                {
                    state = BattleState4.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP1 && deadE1 && deadP2 && deadE2 && deadP3)
                {
                    state = BattleState4.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            if (!deadP1)
            {
                state = BattleState4.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP1 && !deadE1)
            {
                state = BattleState4.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadP1 && deadE1 && !deadP2)
            {
                state = BattleState4.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP1 && deadE1 && deadP2 && !deadE2)
            {
                state = BattleState4.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadP1 && deadE1 && deadP2 && deadE2 && !deadP3)
            {
                state = BattleState4.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP1 && deadE1 && deadP2 && deadE2 && deadP3 && !deadE3)
            {
                state = BattleState4.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadP1 && deadE1 && deadP2 && deadE2 && deadP3 && deadE3)
            {
                state = BattleState4.PLAYER4TURN;
                Player4Turn();
            }

        }
    }

    public void Dead()
    {
        if (deadP1 && deadP2 && deadP3 && deadP4)
        {
            state = BattleState4.LOST;
            StartCoroutine(EndBattle());
        }

        if (deadE1 && deadE2 && deadE3 && deadE4)
        {
            state = BattleState4.WON;
            StartCoroutine(EndBattle());
        }
    }

    IEnumerator EndBattle()
    {
        if (state == BattleState4.WON)
        {
            dialogueText.text = "Voc� Venceu, Parab�ns";
            Destroy(sE1);
            Destroy(sE2);
            Destroy(sE3);
            Destroy(sE4);
            yield return new WaitForSeconds(2f);
            SceneManager.LoadScene("SelecaoFases");
        }
        else if (state == BattleState4.LOST)
        {
            dialogueText.text = "Voc� Perdeu";
            Destroy(sE1);
            Destroy(sE2);
            Destroy(sE3);
            Destroy(sE4);
            yield return new WaitForSeconds(2f);
            SceneManager.LoadScene("SelecaoFases");
        }
    }
}
