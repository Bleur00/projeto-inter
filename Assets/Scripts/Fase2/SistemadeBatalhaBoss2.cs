using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleStateBoss2 { START, PLAYERTURN, BOSSTURN, PLAYER2TURN, PLAYER3TURN, WON, LOST }

public class SistemadeBatalhaBoss2 : MonoBehaviour
{
    public BattleStateBoss2 state;

    [Header("Personagens")]
    public GameObject player;
    public GameObject player2;
    public GameObject player3;
    public GameObject boss;

    [Header("Bot�es")]
    public GameObject buttonAttack;
    private bool showButtonAttack;
    public GameObject buttonAttack2;
    private bool showButtonAttack2;
    public GameObject buttonAttack3;
    private bool showButtonAttack3;
    public GameObject buttonCure;
    private bool showButtonCure;
    public GameObject buttonCure2;
    private bool showButtonCure2;
    public GameObject buttonCure3;
    private bool showButtonCure3;
    public GameObject sB;

    [Header("HUD")]
    [SerializeField]
    CharHUD playerHUD;
    [SerializeField]
    CharHUD player2HUD;
    [SerializeField]
    CharHUD player3HUD;
    [SerializeField]
    CharHUD bossHUD;
    public Text dialogueText;
    public GameObject imageSB;
    public GameObject imageSB2;
    public GameObject imageSB3;
    public GameObject tP1;
    public GameObject tP2;
    public GameObject tP3;

    [Header("Telas")]
    public GameObject telaLost;
    public GameObject telaWon;

    [Header("Sons")]
    public AudioSource[] Som;

    [Header("Anima��o")]
    public static bool bossIdle;


    Personagens playerCharacter;
    Personagens playerCharacter2;
    Personagens playerCharacter3;
    Personagens bossCharacter;

    private bool deadP1;
    private bool deadP2;
    private bool deadP3;
    private bool deadBoss;

    public int randomCharacter;
    public int randomCharacter2;
    public int attackPlayer;

    public bool selectedBoss;

    public bool p1atk;
    public bool p2atk;
    public bool p3atk;

    // Start is called before the first frame update
    void Start()
    {
        state = BattleStateBoss2.START;
        StartCoroutine(SetupBattle());

        deadP1 = false;
        deadP2 = false;
        deadP3 = false;
        deadBoss = false;
        showButtonAttack = false;
        showButtonAttack2 = false;
        showButtonAttack3 = false;
        showButtonCure = false;
        showButtonCure2 = false;
        showButtonCure3 = false;
        selectedBoss = false;
        p1atk = false;
        p2atk = false;
        p3atk = false;
        bossIdle = false;
        telaWon.SetActive(false);
        telaLost.SetActive(false);


        attackPlayer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        playerHUD.SetHUD(playerCharacter);
        player2HUD.SetHUD(playerCharacter2);
        player3HUD.SetHUD(playerCharacter3);
        bossHUD.SetHUD(bossCharacter);

        Dead();

        if (showButtonAttack)
        {
            buttonAttack.SetActive(true);
        }
        else
        {
            buttonAttack.SetActive(false);
        }

        if (showButtonAttack2)
        {
            buttonAttack2.SetActive(true);
        }
        else
        {
            buttonAttack2.SetActive(false);
        }

        if (showButtonAttack3)
        {
            buttonAttack3.SetActive(true);
        }
        else
        {
            buttonAttack3.SetActive(false);
        }

        if (showButtonCure)
        {
            buttonCure.SetActive(true);
        }
        else
        {
            buttonCure.SetActive(false);
        }

        if (showButtonCure2)
        {
            buttonCure2.SetActive(true);
        }
        else
        {
            buttonCure2.SetActive(false);
        }

        if (showButtonCure3)
        {
            buttonCure3.SetActive(true);
        }
        else
        {
            buttonCure3.SetActive(false);
        }

        if (state == BattleStateBoss2.PLAYERTURN && selectedBoss)
        {
            imageSB.SetActive(true);
        }
        else
        {
            imageSB.SetActive(false);
        }

        if (state == BattleStateBoss2.PLAYER2TURN && selectedBoss)
        {
            imageSB2.SetActive(true);
        }
        else
        {
            imageSB2.SetActive(false);
        }

        if (state == BattleStateBoss2.PLAYER3TURN && selectedBoss)
        {
            imageSB3.SetActive(true);
        }
        else
        {
            imageSB3.SetActive(false);
        }


        if (playerCharacter.atualHp <= 0)
        {
            attackPlayer = 1;
        }
        if (playerCharacter2.atualHp <= 0)
        {
            attackPlayer = 2;
        }
        if (playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 3;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter2.atualHp <= 0)
        {
            attackPlayer = 4;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 5;
        }
        if (playerCharacter2.atualHp <= 0 && playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 6;
        }

        Turn();

    }

    IEnumerator SetupBattle()
    {
        GameObject playerGO = player;
        playerCharacter = playerGO.GetComponent<Personagens>();
        GameObject player2GO = player2;
        playerCharacter2 = player2GO.GetComponent<Personagens>();
        GameObject player3GO = player3;
        playerCharacter3 = player3GO.GetComponent<Personagens>();
        GameObject bossGO = boss;
        bossCharacter = bossGO.GetComponent<Personagens>();

        dialogueText.text = "START";

        yield return new WaitForSeconds(2f);

        bossIdle = true;
        state = BattleStateBoss2.PLAYERTURN;
        PlayerTurn();
    }

    void PlayerTurn()
    {
        dialogueText.text = "SEU TURNO";

        if (playerCharacter.atualHp < playerCharacter.maxHp)
        {
            showButtonCure = true;
        }
        else
        {
            showButtonCure = false;
        }
    }

    void Player2Turn()
    {
        dialogueText.text = "SEU TURNO";

        if (playerCharacter2.atualHp < playerCharacter2.maxHp)
        {
            showButtonCure2 = true;
        }
        else
        {
            showButtonCure2 = false;
        }
    }

    void Player3Turn()
    {
        dialogueText.text = "SEU TURNO";

        if (playerCharacter3.atualHp < playerCharacter3.maxHp)
        {
            showButtonCure3 = true;
        }
        else
        {
            showButtonCure3 = false;
        }
    }
    public void SelecionarInimigo()
    {
        if (state == BattleStateBoss2.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if (state == BattleStateBoss2.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        else if (state == BattleStateBoss2.PLAYER3TURN)
        {
            showButtonAttack3 = true;
        }
        Som[0].Play();
        dialogueText.text = "ATAQUE";
        selectedBoss = true;
    }


    public void Toque()
    {
        if (state != BattleStateBoss2.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack());
    }

    public void Toque2()
    {
        if (state != BattleStateBoss2.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack2());
    }

    public void Toque3()
    {
        if (state != BattleStateBoss2.PLAYER3TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack3());
    }

    public void ToqueCura()
    {
        if (state != BattleStateBoss2.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal());
    }

    public void ToqueCura2()
    {
        if (state != BattleStateBoss2.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal2());
    }

    public void ToqueCura3()
    {
        if (state != BattleStateBoss2.PLAYER3TURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal3());
    }

    IEnumerator PlayerHeal()
    {
        selectedBoss = false;
        showButtonAttack = false;
        showButtonCure = false;
        p1atk = true;
        p2atk = false;
        p3atk = false;
        playerCharacter.Cura(9);
        Som[2].Play();
        playerHUD.SetHP(playerCharacter.atualHp);

        yield return new WaitForSeconds(2f);

        state = BattleStateBoss2.BOSSTURN;
        StartCoroutine(BossTurn());
    }


    IEnumerator PlayerHeal2()
    {
        selectedBoss = false;
        showButtonAttack2 = false;
        showButtonCure2 = false;
        p1atk = false;
        p2atk = true;
        p3atk = false;
        playerCharacter2.Cura(9);
        Som[2].Play();
        player2HUD.SetHP(playerCharacter2.atualHp);

        yield return new WaitForSeconds(2f);

        state = BattleStateBoss2.BOSSTURN;
        StartCoroutine(BossTurn());
    }

    IEnumerator PlayerHeal3()
    {
        selectedBoss = false;
        showButtonAttack3 = false;
        showButtonCure3 = false;
        p1atk = false;
        p2atk = false;
        p3atk = true;
        playerCharacter3.Cura(9);
        Som[2].Play();
        player3HUD.SetHP(playerCharacter3.atualHp);

        yield return new WaitForSeconds(2f);

        state = BattleStateBoss2.BOSSTURN;
        StartCoroutine(BossTurn());
    }

    IEnumerator PlayerAttack()
    {
        selectedBoss = false;
        showButtonAttack = false;
        showButtonCure = false;
        p1atk = true;
        p2atk = false;
        p3atk = false;
        Som[1].Play();
        yield return new WaitForSeconds(2.5f);

        bool isDead = bossCharacter.Dano(playerCharacter.dano);

        if (isDead)
        {
            state = BattleStateBoss2.WON;
            StartCoroutine(EndBattle());
        }
        else
        {
            state = BattleStateBoss2.BOSSTURN;
            StartCoroutine(BossTurn());
        }
    }

    IEnumerator PlayerAttack2()
    {
        selectedBoss = false;
        showButtonAttack2 = false;
        showButtonCure2 = false;
        p1atk = false;
        p2atk = true;
        p3atk = false;
        Som[1].Play();
        yield return new WaitForSeconds(2.5f);

        bool isDead = bossCharacter.Dano(playerCharacter2.dano);

        if (isDead)
        {
            state = BattleStateBoss2.WON;
            StartCoroutine(EndBattle());
        }
        else
        {
            state = BattleStateBoss2.BOSSTURN;
            StartCoroutine(BossTurn());
        }
    }

    IEnumerator PlayerAttack3()
    {
        selectedBoss = false;
        showButtonAttack3 = false;
        showButtonCure3 = false;
        p1atk = false;
        p2atk = false;
        p3atk = true;
        Som[1].Play();
        yield return new WaitForSeconds(2.5f);

        bool isDead = bossCharacter.Dano(playerCharacter3.dano);

        if (isDead)
        {
            state = BattleStateBoss2.WON;
            StartCoroutine(EndBattle());
        }
        else
        {
            state = BattleStateBoss2.BOSSTURN;
            StartCoroutine(BossTurn());
        }
    }

    IEnumerator BossTurn()
    {
        dialogueText.text = "TURNO DO INIMIGO";
        if (attackPlayer == 0)
        {
            randomCharacter = Random.Range(1, 4);
        }
        else if (attackPlayer == 1)
        {
            randomCharacter = Random.Range(2, 4);
        }
        else if (attackPlayer == 2)
        {
            randomCharacter2 = Random.Range(1, 3);
            if (randomCharacter2 == 1)
            {
                randomCharacter = 1;
            }
            else if(randomCharacter2 == 2)
            {
                randomCharacter = 3;
            }
        }
        else if (attackPlayer == 3)
        {
            randomCharacter = Random.Range(1, 3);
        }
        else if (attackPlayer == 4)
        {
            randomCharacter = 3;
        }
        else if (attackPlayer == 5)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 6)
        {
            randomCharacter = 1;
        }
        Som[3].Play();
        yield return new WaitForSeconds(1f);

        if (randomCharacter == 1)
        {
            bool isDead = playerCharacter.Dano(bossCharacter.dano);

            if (isDead)
            {
                deadP1 = true;
                if (p1atk)
                {
                    if (!deadP2)
                    {
                        state = BattleStateBoss2.PLAYER2TURN;
                        Player2Turn();
                    }
                    else if (deadP2 && !deadP3)
                    {
                        state = BattleStateBoss2.PLAYER3TURN;
                        Player3Turn();
                    }
                    else if (deadP2 && deadP3)
                    {
                        state = BattleStateBoss2.LOST;
                        StartCoroutine(EndBattle());
                    }
                }
                else if (p2atk)
                {
                    if (!deadP3)
                    {
                        state = BattleStateBoss2.PLAYER3TURN;
                        Player3Turn();
                    }
                    else if (deadP3 && !deadP2)
                    {
                        state = BattleStateBoss2.PLAYER2TURN;
                        Player2Turn();
                    }
                    else if (deadP3 && deadP2)
                    {
                        state = BattleStateBoss2.LOST;
                        StartCoroutine(EndBattle());
                    }
                }
                else if (p3atk)
                {
                    if (!deadP2)
                    {
                        state = BattleStateBoss2.PLAYER2TURN;
                        Player2Turn();
                    }
                    else if (deadP2 && !deadP3)
                    {
                        state = BattleStateBoss2.PLAYER3TURN;
                        Player3Turn();
                    }
                    else if (deadP2 && deadP3)
                    {
                        state = BattleStateBoss2.LOST;
                        StartCoroutine(EndBattle());
                    }
                }
            }
            else if (p1atk)
            {
                if (!deadP2)
                {
                    state = BattleStateBoss2.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP2 && !deadP3)
                {
                    state = BattleStateBoss2.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP2 && deadP3)
                {
                    state = BattleStateBoss2.PLAYERTURN;
                    PlayerTurn();
                }
            }
            else if (p2atk)
            {
                if (!deadP3)
                {
                    state = BattleStateBoss2.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP3)
                {
                    state = BattleStateBoss2.PLAYERTURN;
                    PlayerTurn();
                }
            }
            else if (p3atk)
            {
                state = BattleStateBoss2.PLAYERTURN;
                PlayerTurn();
            }
        }
        else if (randomCharacter == 2)
        {
            bool isDead = playerCharacter2.Dano(bossCharacter.dano);

            if (isDead)
            {
                deadP2 = true;
                if (p1atk)
                {
                    if (!deadP3)
                    {
                        state = BattleStateBoss2.PLAYER3TURN;
                        Player3Turn();
                    }
                    else if (deadP3 && !deadP1)
                    {
                        state = BattleStateBoss2.PLAYERTURN;
                        PlayerTurn();
                    }
                    else if (deadP3 && deadP1)
                    {
                        state = BattleStateBoss2.LOST;
                        StartCoroutine(EndBattle());
                    }
                }
                else if (p2atk)
                {
                    if (!deadP3)
                    {
                        state = BattleStateBoss2.PLAYER3TURN;
                        Player3Turn();
                    }
                    else if (deadP3 && !deadP1)
                    {
                        state = BattleStateBoss2.PLAYERTURN;
                        PlayerTurn();
                    }
                    else if (deadP3 && deadP1)
                    {
                        state = BattleStateBoss2.LOST;
                        StartCoroutine(EndBattle());
                    }
                }
                else if (p3atk)
                {
                    if (!deadP3)
                    {
                        state = BattleStateBoss2.PLAYER3TURN;
                        Player3Turn();
                    }
                    else if (deadP3 && !deadP1)
                    {
                        state = BattleStateBoss2.PLAYERTURN;
                        PlayerTurn();
                    }
                    else if (deadP3 && deadP1)
                    {
                        state = BattleStateBoss2.LOST;
                        StartCoroutine(EndBattle());
                    }
                }
            }
            else if (p1atk)
            {
                state = BattleStateBoss2.PLAYER2TURN;
                Player2Turn();
            }
            else if (p2atk)
            {
                if (!deadP3)
                {
                    state = BattleStateBoss2.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadP3 && !deadP1)
                {
                    state = BattleStateBoss2.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP3 && deadP1)
                {
                    state = BattleStateBoss2.PLAYER2TURN;
                    Player2Turn();
                }
            }
            else if (p3atk)
            {
                if(!deadP1)
                {
                    state = BattleStateBoss2.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP1)
                {
                    state = BattleStateBoss2.PLAYER2TURN;
                    Player2Turn();
                }
            }
        }
        else if (randomCharacter == 3)
        {
            bool isDead = playerCharacter3.Dano(bossCharacter.dano);

            if (isDead)
            {
                deadP3 = true;
                if (p1atk)
                {
                    if (!deadP2)
                    {
                        state = BattleStateBoss2.PLAYER2TURN;
                        Player2Turn();
                    }
                    else if (deadP2 && !deadP1)
                    {
                        state = BattleStateBoss2.PLAYERTURN;
                        PlayerTurn();
                    }
                    else if (deadP2 && deadP1)
                    {
                        state = BattleStateBoss2.LOST;
                        StartCoroutine(EndBattle());
                    }
                }
                else if (p2atk)
                {
                    if (!deadP1)
                    {
                        state = BattleStateBoss2.PLAYERTURN;
                        PlayerTurn();
                    }
                    else if (deadP1 && !deadP2)
                    {
                        state = BattleStateBoss2.PLAYER2TURN;
                        Player2Turn();
                    }
                    else if (deadP1 && deadP2)
                    {
                        state = BattleStateBoss2.LOST;
                        StartCoroutine(EndBattle());
                    }
                }
                else if (p3atk)
                {
                    if (!deadP1)
                    {
                        state = BattleStateBoss2.PLAYERTURN;
                        PlayerTurn();
                    }
                    else if (deadP1 && !deadP2)
                    {
                        state = BattleStateBoss2.PLAYER2TURN;
                        Player2Turn();
                    }
                    else if (deadP1 && deadP2)
                    {
                        state = BattleStateBoss2.LOST;
                        StartCoroutine(EndBattle());
                    }
                }
            }
            else if (p1atk)
            {
                if (!deadP2)
                {
                    state = BattleStateBoss2.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP2)
                {
                    state = BattleStateBoss2.PLAYER3TURN;
                    Player3Turn();
                }
            }
            else if (p2atk)
            {
                state = BattleStateBoss2.PLAYER3TURN;
                Player3Turn();
            }
            else if (p3atk)
            {
                if (!deadP1)
                {
                    state = BattleStateBoss2.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadP1 && !deadP2)
                {
                    state = BattleStateBoss2.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadP1 && deadP2)
                {
                    state = BattleStateBoss2.PLAYER3TURN;
                    Player3Turn();
                }
            }
        }
    }

    public void Dead()
    {
        if (deadP1 && deadP2 && deadP3)
        {
            state = BattleStateBoss2.LOST;
            StartCoroutine(EndBattle());
        }

        if (deadBoss)
        {
            state = BattleStateBoss2.WON;
            StartCoroutine(EndBattle());
        }
    }

    public void Turn()
    {
        if (state == BattleStateBoss2.PLAYERTURN)
        {
            tP1.SetActive(true);
        }
        else
        {
            tP1.SetActive(false);
        }

        if (state == BattleStateBoss2.PLAYER2TURN)
        {
            tP2.SetActive(true);
        }
        else
        {
            tP2.SetActive(false);
        }

        if (state == BattleStateBoss2.PLAYER2TURN)
        {
            tP3.SetActive(true);
        }
        else
        {
            tP3.SetActive(false);
        }
    }

    IEnumerator EndBattle()
    {
        if (state == BattleStateBoss2.WON)
        {
            dialogueText.text = "VOC� VENCEU, PARAB�NS !!!";
            yield return new WaitForSeconds(2f);
            if (FasesManager.fase == 9)
            {
                FasesManager.fase = 10;
                PlayerPrefs.SetInt("fase", FasesManager.fase);
            }
            telaWon.SetActive(true);
        }
        else if (state == BattleStateBoss2.LOST)
        {
            dialogueText.text = "VOC� PERDEU, TENTE NOVAMENTE";
            yield return new WaitForSeconds(2f);
            telaLost.SetActive(true);
        }
    }
}
