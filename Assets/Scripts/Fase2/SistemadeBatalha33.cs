using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleState33 { START, PLAYERTURN, ENEMYTURN, PLAYER2TURN, ENEMY2TURN, PLAYER3TURN, ENEMY3TURN, WON, LOST }
public class SistemadeBatalha33 : MonoBehaviour
{
    public BattleState33 state;

    [Header("Personagens")]
    public GameObject player;
    public GameObject player2;
    public GameObject player3;
    public GameObject enemy;
    public GameObject enemy2;
    public GameObject enemy3;

    [Header("Bot�es")]
    public GameObject buttonAttack;
    private bool showButtonAttack;
    public GameObject buttonAttack2;
    private bool showButtonAttack2;
    public GameObject buttonAttack3;
    private bool showButtonAttack3;
    public GameObject buttonCure;
    private bool showButtonCure;
    public GameObject buttonCure2;
    private bool showButtonCure2;
    public GameObject buttonCure3;
    private bool showButtonCure3;
    public GameObject sE1;
    public GameObject sE2;
    public GameObject sE3;

    [Header("HUD")]
    [SerializeField]
    CharHUD playerHUD;
    [SerializeField]
    CharHUD enemyHUD;
    [SerializeField]
    CharHUD player2HUD;
    [SerializeField]
    CharHUD enemy2HUD;
    [SerializeField]
    CharHUD player3HUD;
    [SerializeField]
    CharHUD enemy3HUD;
    public Text dialogueText;
    public GameObject imageSE1P1;
    public GameObject imageSE2P1;
    public GameObject imageSE3P1;
    public GameObject imageSE1P2;
    public GameObject imageSE2P2;
    public GameObject imageSE3P2;
    public GameObject imageSE1P3;
    public GameObject imageSE2P3;
    public GameObject imageSE3P3;
    public GameObject tP1;
    public GameObject tP2;
    public GameObject tP3;

    [Header("Telas")]
    public GameObject telaLost;
    public GameObject telaWon;

    [Header("Sons")]
    public AudioSource[] Som;

    Personagens playerCharacter;
    Personagens playerCharacter2;
    Personagens playerCharacter3;
    Personagens enemyCharacter;
    Personagens enemyCharacter2;
    Personagens enemyCharacter3;

    private bool deadP1;
    private bool deadP2;
    private bool deadP3;
    private bool deadE1;
    private bool deadE2;
    private bool deadE3;

    public int randomCharacter;
    public int attackPlayer;

    public bool selectedEnemy1;
    public bool selectedEnemy2;
    public bool selectedEnemy3;

    // Start is called before the first frame update
    void Start()
    {
        state = BattleState33.START;
        StartCoroutine(SetupBattle());

        deadP1 = false;
        deadP2 = false;
        deadP3 = false;
        deadE1 = false;
        deadE2 = false;
        deadE3 = false;
        showButtonAttack = false;
        showButtonAttack2 = false;
        showButtonAttack3 = false;
        showButtonCure = false;
        showButtonCure2 = false;
        showButtonCure3 = false;
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        telaWon.SetActive(false);
        telaLost.SetActive(false);

        attackPlayer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        playerHUD.SetHUD(playerCharacter);
        player2HUD.SetHUD(playerCharacter2);
        player3HUD.SetHUD(playerCharacter3);
        enemyHUD.SetHUD(enemyCharacter);
        enemy2HUD.SetHUD(enemyCharacter2);
        enemy3HUD.SetHUD(enemyCharacter3);

        Dead();

        if (showButtonAttack)
        {
            buttonAttack.SetActive(true);
        }
        else
        {
            buttonAttack.SetActive(false);
        }

        if (showButtonAttack2)
        {
            buttonAttack2.SetActive(true);
        }
        else
        {
            buttonAttack2.SetActive(false);
        }

        if (showButtonAttack3)
        {
            buttonAttack3.SetActive(true);
        }
        else
        {
            buttonAttack3.SetActive(false);
        }

        if (showButtonCure)
        {
            buttonCure.SetActive(true);
        }
        else
        {
            buttonCure.SetActive(false);
        }

        if (showButtonCure2)
        {
            buttonCure2.SetActive(true);
        }
        else
        {
            buttonCure2.SetActive(false);
        }

        if (showButtonCure3)
        {
            buttonCure3.SetActive(true);
        }
        else
        {
            buttonCure3.SetActive(false);
        }

        if (playerCharacter.atualHp <= 0)
        {
            attackPlayer = 1;
        }
        if (playerCharacter2.atualHp <= 0)
        {
            attackPlayer = 2;
        }
        if (playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 3;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter2.atualHp <= 0)
        {
            attackPlayer = 4;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 5;
        }
        if (playerCharacter2.atualHp <= 0 && playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 6;
        }

        if (state == BattleState33.PLAYERTURN && selectedEnemy1)
        {
            imageSE1P1.SetActive(true);
        }
        else
        {
            imageSE1P1.SetActive(false);
        }
        if (state == BattleState33.PLAYERTURN && selectedEnemy2)
        {
            imageSE2P1.SetActive(true);
        }
        else
        {
            imageSE2P1.SetActive(false);
        }
        if (state == BattleState33.PLAYERTURN && selectedEnemy3)
        {
            imageSE3P1.SetActive(true);
        }
        else
        {
            imageSE3P1.SetActive(false);
        }
        if (state == BattleState33.PLAYER2TURN && selectedEnemy1)
        {
            imageSE1P2.SetActive(true);
        }
        else
        {
            imageSE1P2.SetActive(false);
        }
        if (state == BattleState33.PLAYER2TURN && selectedEnemy2)
        {
            imageSE2P2.SetActive(true);
        }
        else
        {
            imageSE2P2.SetActive(false);
        }
        if (state == BattleState33.PLAYER2TURN && selectedEnemy3)
        {
            imageSE3P2.SetActive(true);
        }
        else
        {
            imageSE3P2.SetActive(false);
        }
        if (state == BattleState33.PLAYER3TURN && selectedEnemy1)
        {
            imageSE1P3.SetActive(true);
        }
        else
        {
            imageSE1P3.SetActive(false);
        }
        if (state == BattleState33.PLAYER3TURN && selectedEnemy2)
        {
            imageSE2P3.SetActive(true);
        }
        else
        {
            imageSE2P3.SetActive(false);
        }
        if (state == BattleState33.PLAYER3TURN && selectedEnemy3)
        {
            imageSE3P3.SetActive(true);
        }
        else
        {
            imageSE3P3.SetActive(false);
        }

        if (deadE1)
        {
            selectedEnemy1 = false;
        }
        if (deadE2)
        {
            selectedEnemy2 = false;
        }
        if (deadE3)
        {
            selectedEnemy3 = false;
        }

        Turn();
    }

    IEnumerator SetupBattle()
    {
        GameObject playerGO = player;
        playerCharacter = playerGO.GetComponent<Personagens>();
        GameObject player2GO = player2;
        playerCharacter2 = player2GO.GetComponent<Personagens>();
        GameObject player3GO = player3;
        playerCharacter3 = player3GO.GetComponent<Personagens>();
        GameObject enemyGO = enemy;
        enemyCharacter = enemyGO.GetComponent<Personagens>();
        GameObject enemy2GO = enemy2;
        enemyCharacter2 = enemy2GO.GetComponent<Personagens>();
        GameObject enemy3GO = enemy3;
        enemyCharacter3 = enemy3GO.GetComponent<Personagens>();

        dialogueText.text = "START";

        yield return new WaitForSeconds(2f);

        state = BattleState33.PLAYERTURN;
        PlayerTurn();
    }

    void PlayerTurn()
    {
        dialogueText.text = "SEU TURNO";

        if (playerCharacter.atualHp < playerCharacter.maxHp)
        {
            showButtonCure = true;
        }
        else
        {
            showButtonCure = false;
        }
    }

    void Player2Turn()
    {
        dialogueText.text = "SEU TURNO";

        if (playerCharacter2.atualHp < playerCharacter2.maxHp)
        {
            showButtonCure2 = true;
        }
        else
        {
            showButtonCure2 = false;
        }
    }

    void Player3Turn()
    {
        dialogueText.text = "SEU TURNO";

        if (playerCharacter3.atualHp < playerCharacter3.maxHp)
        {
            showButtonCure3 = true;
        }
        else
        {
            showButtonCure3 = false;
        }
    }

    public void SelecionarInimigo1()
    {
        if (state == BattleState33.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if (state == BattleState33.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        else if (state == BattleState33.PLAYER3TURN)
        {
            showButtonAttack3 = true;
        }
        Som[0].Play();
        dialogueText.text = "ATAQUE";
        selectedEnemy1 = true;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
    }

    public void SelecionarInimigo2()
    {
        if (state == BattleState33.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if (state == BattleState33.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        else if (state == BattleState33.PLAYER3TURN)
        {
            showButtonAttack3 = true;
        }
        Som[0].Play();
        dialogueText.text = "ATAQUE";
        selectedEnemy1 = false;
        selectedEnemy2 = true;
        selectedEnemy3 = false;
    }

    public void SelecionarInimigo3()
    {
        if (state == BattleState33.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if (state == BattleState33.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        else if (state == BattleState33.PLAYER3TURN)
        {
            showButtonAttack3 = true;
        }
        Som[0].Play();
        dialogueText.text = "ATAQUE";
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = true;
    }

    public void Toque()
    {
        if (state != BattleState33.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack());
    }

    public void Toque2()
    {
        if (state != BattleState33.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack2());
    }

    public void Toque3()
    {
        if (state != BattleState33.PLAYER3TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack3());
    }

    public void ToqueCura()
    {
        if (state != BattleState33.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal());
    }

    public void ToqueCura2()
    {
        if (state != BattleState33.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal2());
    }

    public void ToqueCura3()
    {
        if (state != BattleState33.PLAYER3TURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal3());
    }

    IEnumerator PlayerHeal()
    {
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        showButtonAttack = false;
        showButtonCure = false;
        playerCharacter.Cura(11);
        Som[2].Play();
        playerHUD.SetHP(playerCharacter.atualHp);

        yield return new WaitForSeconds(2f);

        if (!deadE1)
        {
            state = BattleState33.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
        else if (deadE1 && !deadP2)
        {
            state = BattleState33.PLAYER2TURN;
            Player2Turn();
        }
        else if (deadE1 && deadP2 && !deadE2)
        {
            state = BattleState33.ENEMY2TURN;
            StartCoroutine(Enemy2Turn());
        }
        else if (deadE1 && deadP2 && deadE2 && !deadP3)
        {
            state = BattleState33.PLAYER3TURN;
            Player3Turn();
        }
        else if (deadE1 && deadP2 && deadE2 && deadP3 && !deadE3)
        {
            state = BattleState33.ENEMY3TURN;
            StartCoroutine(Enemy3Turn());
        }
    }

    IEnumerator PlayerHeal2()
    {
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        showButtonAttack2 = false;
        showButtonCure2 = false;
        playerCharacter2.Cura(11);
        Som[2].Play();
        player2HUD.SetHP(playerCharacter2.atualHp);

        yield return new WaitForSeconds(2f);

        if (!deadE2)
        {
            state = BattleState33.ENEMY2TURN;
            StartCoroutine(Enemy2Turn());
        }
        else if (deadE2 && !deadP3)
        {
            state = BattleState33.PLAYER3TURN;
            Player3Turn();
        }
        else if (deadE2 && deadP3 && !deadE3)
        {
            state = BattleState33.ENEMY3TURN;
            StartCoroutine(Enemy3Turn());
        }
        else if (deadE2 && deadP3 && deadE3 && !deadP1)
        {
            state = BattleState33.PLAYERTURN;
            PlayerTurn();
        }
        else if (deadE2 && deadP3 && deadE3 && deadP1 && !deadE1)
        {
            state = BattleState33.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }

    IEnumerator PlayerHeal3()
    {
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
        showButtonAttack3 = false;
        showButtonCure3 = false;
        playerCharacter3.Cura(11);
        Som[2].Play();
        player3HUD.SetHP(playerCharacter3.atualHp);

        yield return new WaitForSeconds(2f);

        if (!deadE3)
        {
            state = BattleState33.ENEMY3TURN;
            StartCoroutine(Enemy3Turn());
        }
        else if (deadE3 && !deadP1)
        {
            state = BattleState33.PLAYERTURN;
            PlayerTurn();
        }
        else if (deadE3 && deadP1 && !deadE1)
        {
            state = BattleState33.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
        else if (deadE3 && deadP1 && deadE1 && !deadP2)
        {
            state = BattleState33.PLAYER2TURN;
            Player2Turn();
        }
        else if (deadE3 && deadP1 && deadE1 && deadP2 && !deadE2)
        {
            state = BattleState33.ENEMY2TURN;
            StartCoroutine(Enemy2Turn());
        }
    }

    IEnumerator PlayerAttack()
    {
        showButtonAttack = false;
        showButtonCure = false;
        Som[1].Play();
        yield return new WaitForSeconds(2.5f);

        if (selectedEnemy1)
        {
            bool isDead = enemyCharacter.Dano(playerCharacter.dano);

            if (isDead)
            {
                deadE1 = true;
                if (deadE1 && !deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.WON;
                    StartCoroutine(EndBattle());
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && !deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
            }
            else
            {
                state = BattleState33.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
        }
        else if (selectedEnemy2)
        {
            bool isDead = enemyCharacter2.Dano(playerCharacter.dano);

            if (isDead)
            {
                deadE2 = true;
                if (!deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.WON;
                    StartCoroutine(EndBattle());
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
            }
            else if (!deadE1)
            {
                state = BattleState33.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE1 && !deadP2)
            {
                state = BattleState33.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadE1 && deadP2)
            {
                state = BattleState33.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
        }
        else if (selectedEnemy3)
        {
            bool isDead = enemyCharacter3.Dano(playerCharacter.dano);

            if (isDead)
            {
                deadE3 = true;
                if (!deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE1)
            {
                state = BattleState33.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE1 && !deadP2)
            {
                state = BattleState33.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadE1 && deadP2 && !deadE2)
            {
                state = BattleState33.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadE1 && deadP2 && deadE2 && !deadP3)
            {
                state = BattleState33.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadE1 && deadP2 && deadE2 && deadP3)
            {
                state = BattleState33.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
        }

        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
    }

    IEnumerator PlayerAttack2()
    {
        showButtonAttack2 = false;
        showButtonCure2 = false;
        Som[1].Play();
        yield return new WaitForSeconds(2.5f);

        if (selectedEnemy1)
        {
            bool isDead = enemyCharacter.Dano(playerCharacter2.dano);

            if (isDead)
            {
                deadE1 = true;
                if (deadE1 && !deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && !deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE2)
            {
                state = BattleState33.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadE2 && !deadP3)
            {
                state = BattleState33.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadE2 && deadP3 && !deadE3)
            {
                state = BattleState33.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadE2 && deadP3 && deadE3)
            {
                state = BattleState33.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
        }
        else if (selectedEnemy2)
        {
            bool isDead = enemyCharacter2.Dano(playerCharacter2.dano);

            if (isDead)
            {
                deadE2 = true;
                if (!deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState33.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
        }
        else if (selectedEnemy3)
        {
            bool isDead = enemyCharacter3.Dano(playerCharacter2.dano);

            if (isDead)
            {
                deadE3 = true;
                if (!deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE2)
            {
                state = BattleState33.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadE2 && !deadP3)
            {
                state = BattleState33.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadE2 && deadP3)
            {
                state = BattleState33.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
        }

        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
    }

    IEnumerator PlayerAttack3()
    {
        showButtonAttack3 = false;
        showButtonCure3 = false;
        Som[1].Play();
        yield return new WaitForSeconds(2.5f);

        if (selectedEnemy1)
        {
            bool isDead = enemyCharacter.Dano(playerCharacter3.dano);

            if (isDead)
            {
                deadE1 = true;
                if (deadE1 && !deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && !deadE2 && deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE3)
            {
                state = BattleState33.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadE3 && !deadP1)
            {
                state = BattleState33.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE3 && deadP1)
            {
                state = BattleState33.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
        }
        else if (selectedEnemy2)
        {
            bool isDead = enemyCharacter2.Dano(playerCharacter3.dano);

            if (isDead)
            {
                deadE2 = true;
                if (!deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE3)
            {
                state = BattleState33.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadE3 && !deadP1)
            {
                state = BattleState33.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE3 && deadP1 && !deadE1)
            {
                state = BattleState33.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE3 && deadP1 && deadE1 && !deadP2)
            {
                state = BattleState33.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadE3 && deadP1 && deadE1 && deadP2)
            {
                state = BattleState33.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
        }
        else if (selectedEnemy3)
        {
            bool isDead = enemyCharacter3.Dano(playerCharacter3.dano);

            if (isDead)
            {
                deadE3 = true;
                if (!deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState33.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
        }

        selectedEnemy1 = false;
        selectedEnemy2 = false;
        selectedEnemy3 = false;
    }

    IEnumerator EnemyTurn()
    {
        dialogueText.text = "TURNO DO INIMIGO";
        if (attackPlayer == 0)
        {
            randomCharacter = Random.Range(1, 4);
        }
        else if (attackPlayer == 1)
        {
            randomCharacter = Random.Range(2, 4);
        }
        else if (attackPlayer == 2)
        {
            randomCharacter = Random.Range(1, 4);
            if (randomCharacter == 2)
            {
                randomCharacter = Random.Range(1, 4);
            }
        }
        else if (attackPlayer == 3)
        {
            randomCharacter = Random.Range(1, 3);
        }
        else if (attackPlayer == 4)
        {
            randomCharacter = 3;
        }
        else if (attackPlayer == 5)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 6)
        {
            randomCharacter = 1;
        }
        Som[3].Play();
        yield return new WaitForSeconds(1f);

        if (randomCharacter == 1)
        {
            bool isDead = playerCharacter.Dano(enemyCharacter.dano);

            if (isDead)
            {
                deadP1 = true;
                if (!deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadP2)
            {
                state = BattleState33.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP2 && !deadE2)
            {
                state = BattleState33.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadP2 && deadE2 && !deadP3)
            {
                state = BattleState33.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP2 && deadE2 && deadP3 && !deadE3)
            {
                state = BattleState33.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadP2 && deadE2 && deadP3 && deadE3)
            {
                state = BattleState33.PLAYERTURN;
                PlayerTurn();
            }
        }
        else if (randomCharacter == 2)
        {
            bool isDead = playerCharacter2.Dano(enemyCharacter.dano);

            if (isDead)
            {
                deadP2 = true;
                if (!deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && !deadE2 && deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState33.PLAYER2TURN;
                Player2Turn();
            }
        }
        else if (randomCharacter == 3)
        {
            bool isDead = playerCharacter3.Dano(enemyCharacter.dano);

            if (isDead)
            {
                deadP3 = true;
                if (!deadE1 && !deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && !deadE2 && deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.ENEMY3TURN;
                    StartCoroutine(Enemy3Turn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && deadE2 && deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && deadE2 && deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadP2)
            {
                state = BattleState33.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP2 && !deadE2)
            {
                state = BattleState33.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadP2 && deadE2)
            {
                state = BattleState33.PLAYER3TURN;
                Player3Turn();
            }
        }

    }

    IEnumerator Enemy2Turn()
    {
        dialogueText.text = "TURNO DO INIMIGO";

        Som[3].Play();
        yield return new WaitForSeconds(1f);

        bool isDead = playerCharacter.Dano(enemyCharacter2.dano);
        bool isDead2 = playerCharacter2.Dano(enemyCharacter2.dano);
        bool isDead3 = playerCharacter3.Dano(enemyCharacter2.dano);

        if (isDead)
        {
            deadP1 = true;
            if (!deadP3)
            {
                state = BattleState33.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP3 && !deadE3)
            {
                state = BattleState33.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadP3 && deadE3 && !deadE1)
            {
                state = BattleState33.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadP3 && deadE3 && deadE1 && !deadP2)
            {
                state = BattleState33.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP3 && deadE3 && deadE1 && deadP2)
            {
                state = BattleState33.LOST;
                StartCoroutine(EndBattle());
            }
        }
        else if (!deadP3)
        {
            state = BattleState33.PLAYER3TURN;
            Player3Turn();
        }
        else if (deadP3 && !deadE3)
        {
            state = BattleState33.ENEMY3TURN;
            StartCoroutine(Enemy3Turn());
        }
        else if (deadP3 && deadE3)
        {
            state = BattleState33.PLAYERTURN;
            PlayerTurn();
        }

        if (isDead2)
        {
            deadP2 = true;
            if (!deadP3)
            {
                state = BattleState33.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP3 && !deadE3)
            {
                state = BattleState33.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadP3 && deadE3 && !deadP1)
            {
                state = BattleState33.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP3 && deadE3 && deadP1)
            {
                state = BattleState33.LOST;
                StartCoroutine(EndBattle());
            }
        }
        else if (!deadP3)
        {
            state = BattleState33.PLAYER3TURN;
            Player3Turn();
        }
        else if (deadP3 && !deadE3)
        {
            state = BattleState33.ENEMY3TURN;
            StartCoroutine(Enemy3Turn());
        }
        else if (deadP3 && deadE3 && !deadP1)
        {
            state = BattleState33.PLAYERTURN;
            PlayerTurn();
        }
        else if (deadP3 && deadE3 && deadP1 && !deadE1)
        {
            state = BattleState33.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
        else if (deadP3 && deadE3 && deadP1 && deadE1)
        {
            state = BattleState33.PLAYERTURN;
            PlayerTurn();
        }

        if (isDead3)
        {
            deadP3 = true;
            if (!deadE3)
            {
                state = BattleState33.ENEMY3TURN;
                StartCoroutine(Enemy3Turn());
            }
            else if (deadE3 && !deadP1)
            {
                state = BattleState33.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE3 && deadP1 && !deadE1)
            {
                state = BattleState33.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE3 && deadP1 && deadE1 && !deadP2)
            {
                state = BattleState33.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE3 && deadP1 && deadE1 && deadP2)
            {
                state = BattleState33.LOST;
                StartCoroutine(EndBattle());
            }
        }
        else
        {
            state = BattleState33.PLAYER3TURN;
            Player3Turn();
        }
    }

    IEnumerator Enemy3Turn()
    {
        dialogueText.text = "TURNO DO INIMIGO";
        if (attackPlayer == 0)
        {
            randomCharacter = Random.Range(1, 4);
        }
        else if (attackPlayer == 1)
        {
            randomCharacter = Random.Range(2, 4);
        }
        else if (attackPlayer == 2)
        {
            randomCharacter = Random.Range(1, 4);
            if (randomCharacter == 2)
            {
                randomCharacter = Random.Range(1, 4);
            }
        }
        else if (attackPlayer == 3)
        {
            randomCharacter = Random.Range(1, 3);
        }
        else if (attackPlayer == 4)
        {
            randomCharacter = 3;
        }
        else if (attackPlayer == 5)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 6)
        {
            randomCharacter = 1;
        }
        Som[3].Play();
        yield return new WaitForSeconds(1f);

        if (randomCharacter == 1)
        {
            bool isDead = playerCharacter.Dano(enemyCharacter3.dano);

            if (isDead)
            {
                deadP1 = true;
                if (!deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState33.PLAYERTURN;
                PlayerTurn();
            }
        }
        else if (randomCharacter == 2)
        {
            bool isDead = playerCharacter2.Dano(enemyCharacter3.dano);

            if (isDead)
            {
                deadP2 = true;
                if (!deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleState33.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadP1)
            {
                state = BattleState33.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP1 && !deadE1)
            {
                state = BattleState33.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadP1 && deadE1)
            {
                state = BattleState33.PLAYER2TURN;
                Player2Turn();
            }
        }
        else if (randomCharacter == 3)
        {
            bool isDead = playerCharacter3.Dano(enemyCharacter3.dano);

            if (isDead)
            {
                deadP3 = true;
                if (!deadE1 && !deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && !deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && !deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && !deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && deadE2 && !deadE3 && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.PLAYERTURN;
                    PlayerTurn();
                }
                else if (deadE1 && deadE2 && !deadE3 && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleState33.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadE1 && !deadE2 && !deadE3 && deadP1 && deadP2 && deadP3)
                {
                    state = BattleState33.LOST;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadP1)
            {
                state = BattleState33.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP1 && !deadE1)
            {
                state = BattleState33.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadP1 && deadE1 && !deadP2)
            {
                state = BattleState33.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP1 && deadE1 && deadP2 && !deadE2)
            {
                state = BattleState33.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadP1 && deadE1 && deadP2 && deadE2)
            {
                state = BattleState33.PLAYER3TURN;
                Player3Turn();
            }
        }

    }

    public void Dead()
    {
        if (deadP1 && deadP2 && deadP3)
        {
            state = BattleState33.LOST;
            StartCoroutine(EndBattle());
        }

        if (deadE1 && deadE2 && deadE3)
        {
            state = BattleState33.WON;
            StartCoroutine(EndBattle());
        }
    }

    public void Turn()
    {
        if (state == BattleState33.PLAYERTURN)
        {
            tP1.SetActive(true);
        }
        else
        {
            tP1.SetActive(false);
        }

        if (state == BattleState33.PLAYER2TURN)
        {
            tP2.SetActive(true);
        }
        else
        {
            tP2.SetActive(false);
        }

        if (state == BattleState33.PLAYER3TURN)
        {
            tP3.SetActive(true);
        }
        else
        {
            tP3.SetActive(false);
        }
    }

    IEnumerator EndBattle()
    {
        if (state == BattleState33.WON)
        {
            dialogueText.text = "VOC� VENCEU, PARAB�NS !!!";
            Destroy(sE1);
            Destroy(sE2);
            Destroy(sE3);
            yield return new WaitForSeconds(2f);
            if (FasesManager.fase == 8)
            {
                FasesManager.fase = 9;
                PlayerPrefs.SetInt("fase", FasesManager.fase);
            }
            telaWon.SetActive(true);
        }
        else if (state == BattleState33.LOST)
        {
            dialogueText.text = "VOC� PERDEU, TENTE NOVAMENTE";
            Destroy(sE1);
            Destroy(sE2);
            Destroy(sE3);
            yield return new WaitForSeconds(2f);
            telaLost.SetActive(true);
        }
    }
}
