using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleState22 { START, PLAYERTURN, ENEMYTURN, PLAYER2TURN, ENEMY2TURN, WON, LOST }
public class SistemadeBatalha2x22 : MonoBehaviour
{
    public BattleState22 state;

    [Header("Personagens")]
    public GameObject player;
    public GameObject player2;
    public GameObject enemy;
    public GameObject enemy2;

    [Header("Bot�es")]
    public GameObject buttonAttack;
    private bool showButtonAttack;
    public GameObject buttonAttack2;
    private bool showButtonAttack2;
    public GameObject buttonCure;
    private bool showButtonCure;
    public GameObject buttonCure2;
    private bool showButtonCure2;
    public GameObject sE1;
    public GameObject sE2;

    [Header("HUD")]
    [SerializeField]
    CharHUD playerHUD;
    [SerializeField]
    CharHUD enemyHUD;
    [SerializeField]
    CharHUD player2HUD;
    [SerializeField]
    CharHUD enemy2HUD;
    public Text dialogueText;
    public GameObject imageSE1P1;
    public GameObject imageSE2P1;
    public GameObject imageSE1P2;
    public GameObject imageSE2P2;
    public GameObject tP1;
    public GameObject tP2;

    [Header("Telas")]
    public GameObject telaLost;
    public GameObject telaWon;

    [Header("Sons")]
    public AudioSource[] Som;

    Personagens playerCharacter;
    Personagens playerCharacter2;
    Personagens enemyCharacter;
    Personagens enemyCharacter2;

    private bool deadP1;
    private bool deadP2;
    private bool deadE1;
    private bool deadE2;

    public bool selectedEnemy1;
    public bool selectedEnemy2;

    // Start is called before the first frame update
    void Start()
    {
        state = BattleState22.START;
        StartCoroutine(SetupBattle());

        deadP1 = false;
        deadP2 = false;
        deadE1 = false;
        deadE2 = false;
        showButtonAttack = false;
        showButtonAttack2 = false;
        showButtonCure = false;
        showButtonCure2 = false;
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        telaWon.SetActive(false);
        telaLost.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        playerHUD.SetHUD(playerCharacter);
        player2HUD.SetHUD(playerCharacter2);
        enemyHUD.SetHUD(enemyCharacter);
        enemy2HUD.SetHUD(enemyCharacter2);

        Dead();

        if (showButtonAttack)
        {
            buttonAttack.SetActive(true);
        }
        else
        {
            buttonAttack.SetActive(false);
        }

        if (showButtonAttack2)
        {
            buttonAttack2.SetActive(true);
        }
        else
        {
            buttonAttack2.SetActive(false);
        }

        if (showButtonCure)
        {
            buttonCure.SetActive(true);
        }
        else
        {
            buttonCure.SetActive(false);
        }

        if (showButtonCure2)
        {
            buttonCure2.SetActive(true);
        }
        else
        {
            buttonCure2.SetActive(false);
        }

        if (state == BattleState22.PLAYERTURN && selectedEnemy1)
        {
            imageSE1P1.SetActive(true);
        }
        else
        {
            imageSE1P1.SetActive(false);
        }
        if (state == BattleState22.PLAYERTURN && selectedEnemy2)
        {
            imageSE2P1.SetActive(true);
        }
        else
        {
            imageSE2P1.SetActive(false);
        }
        if (state == BattleState22.PLAYER2TURN && selectedEnemy1)
        {
            imageSE1P2.SetActive(true);
        }
        else
        {
            imageSE1P2.SetActive(false);
        }
        if (state == BattleState22.PLAYER2TURN && selectedEnemy2)
        {
            imageSE2P2.SetActive(true);
        }
        else
        {
            imageSE2P2.SetActive(false);
        }

        if (deadE1)
        {
            selectedEnemy1 = false;
        }
        if (deadE2)
        {
            selectedEnemy2 = false;
        }

        Turn();
    }

    IEnumerator SetupBattle()
    {
        GameObject playerGO = player;
        playerCharacter = playerGO.GetComponent<Personagens>();
        GameObject player2GO = player2;
        playerCharacter2 = player2GO.GetComponent<Personagens>();
        GameObject enemyGO = enemy;
        enemyCharacter = enemyGO.GetComponent<Personagens>();
        GameObject enemy2GO = enemy2;
        enemyCharacter2 = enemy2GO.GetComponent<Personagens>();

        dialogueText.text = "INFELIZMENTE PARECE QUE AINDA EXISTEM MAIS C�LULAS MALIGNAS POR A�, ISSO N�O � UM BOM SINAL";

        yield return new WaitForSeconds(5f);

        state = BattleState22.PLAYERTURN;
        PlayerTurn();
    }

    void PlayerTurn()
    {
        dialogueText.text = "SEU TURNO";

        if (playerCharacter.atualHp < playerCharacter.maxHp)
        {
            showButtonCure = true;
        }
        else
        {
            showButtonCure = false;
        }
    }

    void Player2Turn()
    {
        dialogueText.text = "SEU TURNO";

        if (playerCharacter2.atualHp < playerCharacter2.maxHp)
        {
            showButtonCure2 = true;
        }
        else
        {
            showButtonCure2 = false;
        }
    }

    public void SelecionarInimigo1()
    {
        if (state == BattleState22.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if (state == BattleState22.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        Som[0].Play();
        dialogueText.text = "ATAQUE";
        selectedEnemy1 = true;
        selectedEnemy2 = false;
    }

    public void SelecionarInimigo2()
    {

        if (state == BattleState22.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else if (state == BattleState22.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        Som[0].Play();
        dialogueText.text = "ATAQUE";
        selectedEnemy2 = true;
        selectedEnemy1 = false;
    }

    public void Toque()
    {
        if (state != BattleState22.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack());
    }

    public void Toque2()
    {
        if (state != BattleState22.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack2());
    }

    public void ToqueCura()
    {
        if (state != BattleState22.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal());
    }

    public void ToqueCura2()
    {
        if (state != BattleState22.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerHeal2());
    }

    IEnumerator PlayerHeal()
    {
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        showButtonAttack = false;
        showButtonCure = false;
        playerCharacter.Cura(7);
        Som[2].Play();
        playerHUD.SetHP(playerCharacter.atualHp);

        yield return new WaitForSeconds(2f);

        if (!deadE1)
        {
            state = BattleState22.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
        else if (deadE1 && !deadP2)
        {
            state = BattleState22.PLAYER2TURN;
            Player2Turn();
        }
        else if (deadE1 && deadP2 && !deadE2)
        {
            state = BattleState22.ENEMY2TURN;
            StartCoroutine(Enemy2Turn());
        }
    }

    IEnumerator PlayerHeal2()
    {
        selectedEnemy1 = false;
        selectedEnemy2 = false;
        showButtonAttack2 = false;
        showButtonCure2 = false;
        playerCharacter2.Cura(7);
        Som[2].Play();
        player2HUD.SetHP(playerCharacter2.atualHp);

        yield return new WaitForSeconds(2f);

        if (!deadE2)
        {
            state = BattleState22.ENEMY2TURN;
            StartCoroutine(Enemy2Turn());
        }
        else if (deadE2 && !deadP1)
        {
            state = BattleState22.PLAYERTURN;
            PlayerTurn();
        }
        else if (deadE2 && deadP1 && !deadE1)
        {
            state = BattleState22.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
    }

    IEnumerator PlayerAttack()
    {
        showButtonAttack = false;
        showButtonCure = false;
        Som[1].Play();
        yield return new WaitForSeconds(2.5f);

        if (selectedEnemy1)
        {
            bool isDead = enemyCharacter.Dano(playerCharacter.dano);

            if (isDead)
            {
                deadE1 = true;
                if (deadE1 && !deadE2 && !deadP1 && !deadP2)
                {
                    state = BattleState22.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadE2 && !deadP1 && !deadP2)
                {
                    state = BattleState22.WON;
                    StartCoroutine(EndBattle());
                }
                else if (deadE1 && deadE2 && !deadP1 && deadP2)
                {
                    state = BattleState22.WON;
                    StartCoroutine(EndBattle());
                }
                else if (deadE1 && !deadE2 && !deadP1 && deadP2)
                {
                    state = BattleState22.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
            }
            else
            {
                state = BattleState22.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
        }
        else if (selectedEnemy2)
        {
            bool isDead = enemyCharacter2.Dano(playerCharacter.dano);

            if (isDead)
            {
                deadE2 = true;
                if (!deadE1 && deadE2 && !deadP1 && !deadP2)
                {
                    state = BattleState22.PLAYER2TURN;
                    Player2Turn();
                }
                else if (deadE1 && deadE2 && !deadP1 && !deadP2)
                {
                    state = BattleState22.WON;
                    StartCoroutine(EndBattle());
                }
                else if (deadE1 && deadE2 && !deadP1 && deadP2)
                {
                    state = BattleState22.WON;
                    StartCoroutine(EndBattle());
                }
                else if (!deadE1 && deadE2 && !deadP1 && deadP2)
                {
                    state = BattleState22.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
            }
            else if (!deadE1)
            {
                state = BattleState22.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (deadE1 && !deadP2)
            {
                state = BattleState22.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadE1 && deadP2)
            {
                state = BattleState22.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
        }

        selectedEnemy1 = false;
        selectedEnemy2 = false;
    }

    IEnumerator PlayerAttack2()
    {
        showButtonAttack2 = false;
        showButtonCure2 = false;
        Som[1].Play();
        yield return new WaitForSeconds(2.5f);

        if (selectedEnemy1)
        {
            bool isDead = enemyCharacter.Dano(playerCharacter2.dano);

            if (isDead)
            {
                deadE1 = true;
                if (deadE1 && deadE2 && !deadP1 && !deadP2)
                {
                    state = BattleState22.WON;
                    StartCoroutine(EndBattle());
                }
                else if (deadE1 && !deadE2 && !deadP1 && !deadP2)
                {
                    state = BattleState22.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && !deadE2 && deadP1 && !deadP2)
                {
                    state = BattleState22.ENEMY2TURN;
                    StartCoroutine(Enemy2Turn());
                }
                else if (deadE1 && deadE2 && deadP1 && !deadP2)
                {
                    state = BattleState22.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else if (!deadE2)
            {
                state = BattleState22.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (deadE2 && !deadP1)
            {
                state = BattleState22.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE2 && deadP1)
            {
                state = BattleState22.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
        }

        else if (selectedEnemy2)
        {
            bool isDead = enemyCharacter2.Dano(playerCharacter2.dano);

            if (isDead)
            {
                deadE2 = true;
                if (deadE1 && deadE2 && !deadP1 && !deadP2)
                {
                    state = BattleState22.WON;
                    StartCoroutine(EndBattle());
                }
                else if (!deadE1 && deadE2 && !deadP1 && !deadP2)
                {
                    state = BattleState22.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadE1 && deadE2 && deadP1 && !deadP2)
                {
                    state = BattleState22.ENEMYTURN;
                    StartCoroutine(EnemyTurn());
                }
                else if (deadE1 && deadE2 && deadP1 && !deadP2)
                {
                    state = BattleState22.WON;
                    StartCoroutine(EndBattle());
                }
            }
            else
            {
                state = BattleState22.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
        }

        selectedEnemy1 = false;
        selectedEnemy2 = false;

    }

    IEnumerator EnemyTurn()
    {
        dialogueText.text = "TURNO DO INIMIGO";

        Som[3].Play();
        yield return new WaitForSeconds(1f);

        bool isDead = playerCharacter2.Dano(enemyCharacter.dano);
        bool isDead2 = playerCharacter.Dano(enemyCharacter.dano);

        if (isDead)
        {
            deadP2 = true;
            if (!deadE1 && !deadE2 && !deadP1 && deadP2)
            {
                state = BattleState22.ENEMY2TURN;
                StartCoroutine(Enemy2Turn());
            }
            else if (!deadE1 && !deadE2 && deadP1 && deadP2)
            {
                state = BattleState22.LOST;
                StartCoroutine(EndBattle());
            }
            else if (!deadE1 && deadE2 && !deadP1 && deadP2)
            {
                state = BattleState22.PLAYERTURN;
                PlayerTurn();
            }
            else if (!deadE1 && deadE2 && deadP1 && deadP2)
            {
                state = BattleState22.LOST;
                StartCoroutine(EndBattle());
            }
        }
        else
        {
            state = BattleState22.PLAYER2TURN;
            Player2Turn();
        }

        if (isDead2)
        {
            deadP1 = true;
            if (!deadE1 && !deadE2 && deadP1 && !deadP2)
            {
                state = BattleState22.PLAYER2TURN;
                Player2Turn();
            }
            else if (!deadE1 && !deadE2 && deadP1 && deadP2)
            {
                state = BattleState22.LOST;
                StartCoroutine(EndBattle());
            }
            else if (!deadE1 && deadE2 && deadP1 && !deadP2)
            {
                state = BattleState22.PLAYER2TURN;
                Player2Turn();
            }
            else if (!deadE1 && deadE2 && deadP1 && deadP2)
            {
                state = BattleState22.LOST;
                StartCoroutine(EndBattle());
            }
        }
        else
        {
            state = BattleState22.PLAYER2TURN;
            Player2Turn();
        }
    }

    IEnumerator Enemy2Turn()
    {
        dialogueText.text = "TURNO DO INIMIGO";

        Som[3].Play();
        yield return new WaitForSeconds(1f);

        bool isDead = playerCharacter2.Dano(enemyCharacter2.dano);
        bool isDead2 = playerCharacter.Dano(enemyCharacter2.dano);

        if (isDead)
        {
            deadP2 = true;
            if (!deadE1 && !deadE2 && !deadP1 && deadP2)
            {
                state = BattleState22.PLAYERTURN;
                PlayerTurn();
            }
            else if (!deadE1 && !deadE2 && deadP1 && deadP2)
            {
                state = BattleState22.LOST;
                StartCoroutine(EndBattle());
            }
            else if (deadE1 && !deadE2 && !deadP1 && deadP2)
            {
                state = BattleState22.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadE1 && !deadE2 && deadP1 && deadP2)
            {
                state = BattleState22.LOST;
                StartCoroutine(EndBattle());
            }
        }
        else
        {
            state = BattleState22.PLAYERTURN;
            PlayerTurn();
        }

        if (isDead2)
        {
            deadP1 = true;
            if (!deadE1 && !deadE2 && deadP1 && !deadP2)
            {
                state = BattleState22.ENEMYTURN;
                StartCoroutine(EnemyTurn());
            }
            else if (!deadE1 && !deadE2 && deadP1 && deadP2)
            {
                state = BattleState22.LOST;
                StartCoroutine(EndBattle());
            }
            else if (deadE1 && !deadE2 && deadP1 && !deadP2)
            {
                state = BattleState22.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadE1 && !deadE2 && deadP1 && deadP2)
            {
                state = BattleState22.LOST;
                StartCoroutine(EndBattle());
            }
        }
        else
        {
            state = BattleState22.PLAYERTURN;
            PlayerTurn();
        }

    }

    public void Dead()
    {
        if (deadP1 && deadP2)
        {
            state = BattleState22.LOST;
            StartCoroutine(EndBattle());
        }

        if (deadE1 && deadE2)
        {
            state = BattleState22.WON;
            StartCoroutine(EndBattle());
        }
    }

    public void Turn()
    {
        if (state == BattleState22.PLAYERTURN)
        {
            tP1.SetActive(true);
        }
        else
        {
            tP1.SetActive(false);
        }

        if (state == BattleState22.PLAYER2TURN)
        {
            tP2.SetActive(true);
        }
        else
        {
            tP2.SetActive(false);
        }
    }


    IEnumerator EndBattle()
    {
        if (state == BattleState22.WON)
        {
            dialogueText.text = "VOC� VENCEU, PARAB�NS !!!";
            Destroy(sE1);
            Destroy(sE2);
            yield return new WaitForSeconds(2f);
            if (FasesManager.fase == 5)
            {
                FasesManager.fase = 6;
                PlayerPrefs.SetInt("fase", FasesManager.fase);
            }
            telaWon.SetActive(true);
        }
        else if (state == BattleState22.LOST)
        {
            dialogueText.text = "VOC� PERDEU, TENTE NOVAMENTE";
            Destroy(sE1);
            Destroy(sE2);
            yield return new WaitForSeconds(2f);
            telaLost.SetActive(true);
        }
    }
}

