using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    public GameObject panel;
    public GameObject panel2;
    public GameObject PauseButton;

    [Header("Sons")]
    public AudioSource[] Som;
    // Start is called before the first frame update
    void Start()
    {
        panel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Pause()
    {
        Som[0].Play();
        panel.SetActive(true);

        PauseButton.SetActive(false);

        Time.timeScale = 0;

    }

    public void Play()
    {
        Som[0].Play();
        Time.timeScale = 1;
        panel.SetActive(false);
        panel2.SetActive(false);

        PauseButton.SetActive(true);
    }

    public void Sair()
    {
        Som[1].Play();
        Time.timeScale = 1;
        SceneManager.LoadScene("SelecaoFases");
    }

    public void Certeza()
    {
        Som[0].Play();
        panel2.SetActive(true);
    }
}
