using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum BattleStateBoss { START, PLAYERTURN, BOSSTURN, PLAYER2TURN, PLAYER3TURN, WON, LOST }

public class SistemadeBatalhaBoss : MonoBehaviour
{
    public BattleStateBoss state;

    [Header("Personagens")]
    public GameObject player;
    public GameObject player2;
    public GameObject player3;
    public GameObject boss;

    [Header("Bot�es")]
    public GameObject buttonAttack;
    private bool showButtonAttack;
    public GameObject buttonAttack2;
    private bool showButtonAttack2;
    public GameObject buttonAttack3;
    private bool showButtonAttack3;

    [Header("HUD")]
    [SerializeField]
    CharHUD playerHUD;
    [SerializeField]
    CharHUD player2HUD;
    [SerializeField]
    CharHUD player3HUD;
    [SerializeField]
    CharHUD bossHUD;
    public Text dialogueText;

    Personagens playerCharacter;
    Personagens playerCharacter2;
    Personagens playerCharacter3;
    Personagens bossCharacter;

    private bool deadP1;
    private bool deadP2;
    private bool deadP3;
    private bool deadBoss;

    public int randomCharacter;
    public int attackPlayer;

    // Start is called before the first frame update
    void Start()
    {
        state = BattleStateBoss.START;
        StartCoroutine(SetupBattle());

        deadP1 = false;
        deadP2 = false;
        deadP3 = false;
        deadBoss = false;
        showButtonAttack = false;
        showButtonAttack2 = false;
        showButtonAttack3 = false;

        attackPlayer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        playerHUD.SetHUD(playerCharacter);
        player2HUD.SetHUD(playerCharacter2);
        player3HUD.SetHUD(playerCharacter3);
        bossHUD.SetHUD(bossCharacter);

        if (showButtonAttack)
        {
            buttonAttack.SetActive(true);
        }
        else
        {
            buttonAttack.SetActive(false);
        }

        if (showButtonAttack2)
        {
            buttonAttack2.SetActive(true);
        }
        else
        {
            buttonAttack2.SetActive(false);
        }

        if (showButtonAttack3)
        {
            buttonAttack3.SetActive(true);
        }
        else
        {
            buttonAttack3.SetActive(false);
        }

        if (playerCharacter.atualHp <= 0)
        {
            attackPlayer = 1;
        }
        if (playerCharacter2.atualHp <= 0)
        {
            attackPlayer = 2;
        }
        if (playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 3;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter2.atualHp <= 0)
        {
            attackPlayer = 4;
        }
        if (playerCharacter.atualHp <= 0 && playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 5;
        }
        if (playerCharacter2.atualHp <= 0 && playerCharacter3.atualHp <= 0)
        {
            attackPlayer = 6;
        }

    }

    IEnumerator SetupBattle()
    {
        GameObject playerGO = player;
        playerCharacter = playerGO.GetComponent<Personagens>();
        GameObject player2GO = player2;
        playerCharacter2 = player2GO.GetComponent<Personagens>();
        GameObject player3GO = player3;
        playerCharacter3 = player3GO.GetComponent<Personagens>();
        GameObject bossGO = boss;
        bossCharacter = bossGO.GetComponent<Personagens>();

        dialogueText.text = "Start";

        yield return new WaitForSeconds(2f);

        state = BattleStateBoss.PLAYERTURN;
        PlayerTurn();
    }

    void PlayerTurn()
    {
        dialogueText.text = "Seu turno, Ataque !!!";

        if (state == BattleStateBoss.PLAYERTURN)
        {
            showButtonAttack = true;
        }
        else
        {
            showButtonAttack = false;
        }
    }

    void Player2Turn()
    {
        dialogueText.text = "Seu turno, Ataque !!!";

        if (state == BattleStateBoss.PLAYER2TURN)
        {
            showButtonAttack2 = true;
        }
        else
        {
            showButtonAttack2 = false;
        }
    }

    void Player3Turn()
    {
        dialogueText.text = "Seu turno, Ataque !!!";

        if (state == BattleStateBoss.PLAYER3TURN)
        {
            showButtonAttack3 = true;
        }
        else
        {
            showButtonAttack3 = false;
        }
    }

    public void Toque()
    {
        if (state != BattleStateBoss.PLAYERTURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack());
    }

    public void Toque2()
    {
        if (state != BattleStateBoss.PLAYER2TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack2());
    }

    public void Toque3()
    {
        if (state != BattleStateBoss.PLAYER3TURN)
        {
            return;
        }

        StartCoroutine(PlayerAttack3());
    }

    IEnumerator PlayerAttack()
    {
        showButtonAttack = false;
        yield return new WaitForSeconds(0.1f);

        bool isDead = bossCharacter.Dano(playerCharacter.dano);

        if (isDead)
        {
            state = BattleStateBoss.WON;
            EndBattle();
        }
        else 
        {
            state = BattleStateBoss.BOSSTURN;
            StartCoroutine(BossTurn());
        }
    }

    IEnumerator PlayerAttack2()
    {
        showButtonAttack2 = false;
        yield return new WaitForSeconds(0.1f);

        bool isDead = bossCharacter.Dano(playerCharacter2.dano);

        if (isDead)
        {
            state = BattleStateBoss.WON;
            EndBattle();
        }
        else
        {
            state = BattleStateBoss.BOSSTURN;
            StartCoroutine(BossTurn());
        }
    }

    IEnumerator PlayerAttack3()
    {
        showButtonAttack3 = false;
        yield return new WaitForSeconds(0.1f);

        bool isDead = bossCharacter.Dano(playerCharacter3.dano);

        if (isDead)
        {
            state = BattleStateBoss.WON;
            EndBattle();
        }
        else
        {
            state = BattleStateBoss.BOSSTURN;
            StartCoroutine(BossTurn());
        }
    }

    IEnumerator BossTurn()
    {
        dialogueText.text = "Turno do inimigo";
        if (attackPlayer == 0)
        {
            randomCharacter = Random.Range(1, 4);
        }
        else if (attackPlayer == 1)
        {
            randomCharacter = Random.Range(2, 4);
        }
        else if (attackPlayer == 2)
        {
            randomCharacter = Random.Range(1, 4);
            if (randomCharacter == 2)
            {
                randomCharacter = Random.Range(1, 4);
            }
        }
        else if (attackPlayer == 3)
        {
            randomCharacter = Random.Range(1, 3);
        }
        else if (attackPlayer == 4)
        {
            randomCharacter = 3;
        }
        else if (attackPlayer == 5)
        {
            randomCharacter = 2;
        }
        else if (attackPlayer == 6)
        {
            randomCharacter = 1;
        }

        yield return new WaitForSeconds(1f);

        if (randomCharacter == 1)
        {
            bool isDead = playerCharacter.Dano(bossCharacter.dano);

            if (isDead)
            {
                deadP1 = true;
                if (!deadBoss && deadP1 && !deadP2 && !deadP3)
                {
                    state = BattleStateBoss.PLAYER2TURN;
                    Player2Turn();
                }
                else if(!deadBoss && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleStateBoss.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadBoss && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleStateBoss.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadBoss && deadP1 && deadP2 && deadP3)
                {
                    state = BattleStateBoss.LOST;
                    EndBattle();
                }
            }
            else if (!deadP2)
            {
                state = BattleStateBoss.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP2 && !deadP3)
            {
                state = BattleStateBoss.PLAYER3TURN;
                Player3Turn();
            }
            else if(deadP2 && deadP3)
            {
                state = BattleStateBoss.PLAYERTURN;
                PlayerTurn();
            }
        }
        else if (randomCharacter == 2)
        {
            bool isDead = playerCharacter2.Dano(bossCharacter.dano);

            if (isDead)
            {
                deadP2 = true;
                if (!deadBoss && !deadP1 && deadP2 && !deadP3)
                {
                    state = BattleStateBoss.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadBoss && deadP1 && deadP2 && !deadP3)
                {
                    state = BattleStateBoss.PLAYER3TURN;
                    Player3Turn();
                }
                else if (!deadBoss && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleStateBoss.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadBoss && deadP1 && deadP2 && deadP3)
                {
                    state = BattleStateBoss.LOST;
                    EndBattle();
                }
            }
            else if (!deadP3)
            {
                state = BattleStateBoss.PLAYER3TURN;
                Player3Turn();
            }
            else if (deadP3 && !deadP1)
            {
                state = BattleStateBoss.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP3 && deadP1)
            {
                state = BattleStateBoss.PLAYER2TURN;
                Player2Turn();
            }
        }
        else if (randomCharacter == 3)
        {
            bool isDead = playerCharacter3.Dano(bossCharacter.dano);

            if (isDead)
            {
                deadP3 = true;
                if (!deadBoss && !deadP1 && !deadP2 && deadP3)
                {
                    state = BattleStateBoss.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadBoss && !deadP1 && deadP2 && deadP3)
                {
                    state = BattleStateBoss.PLAYERTURN;
                    PlayerTurn();
                }
                else if (!deadBoss && deadP1 && !deadP2 && deadP3)
                {
                    state = BattleStateBoss.PLAYER2TURN;
                    Player2Turn();
                }
                else if (!deadBoss && deadP1 && deadP2 && deadP3)
                {
                    state = BattleStateBoss.LOST;
                    EndBattle();
                }
            }
            else if (!deadP1)
            {
                state = BattleStateBoss.PLAYERTURN;
                PlayerTurn();
            }
            else if (deadP1 && !deadP2)
            {
                state = BattleStateBoss.PLAYER2TURN;
                Player2Turn();
            }
            else if (deadP1 && deadP2)
            {
                state = BattleStateBoss.PLAYER3TURN;
                Player3Turn();
            }
        }
    }

    void EndBattle()
    {
        if (state == BattleStateBoss.WON)
        {
            dialogueText.text = "Voc� Venceu";
            SceneManager.LoadScene("SelecaoFases");
        }
        else if (state == BattleStateBoss.LOST)
        {
            dialogueText.text = "Voc� Perdeu";
            SceneManager.LoadScene("SelecaoFases");
        }
    }


}

