using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FasesManager : MonoBehaviour
{
    [Header("Bot�es")]
    [SerializeField]
    private GameObject bf1;
    [SerializeField]
    private GameObject bf2;
    [SerializeField]
    private GameObject bf3;
    [SerializeField]
    private GameObject bfb;
    [SerializeField]
    private GameObject bf5;
    [SerializeField]
    private GameObject bf6;
    [SerializeField]
    private GameObject bf7;
    [SerializeField]
    private GameObject bf8;
    [SerializeField]
    private GameObject bfb2;
    [SerializeField]
    private GameObject bf10;
    [SerializeField]
    private GameObject bf11;
    [SerializeField]
    private GameObject bf12;
    [SerializeField]
    private GameObject bf13;
    [SerializeField]
    private GameObject bfb3;

    [Header("Sons")]
    public AudioSource[] Som;

    public bool fase1;
    public bool fase2;
    public bool fase3;
    public bool fase4;
    public bool fase5;
    public bool fase6;
    public bool fase7;
    public bool fase8;
    public bool fase9;
    public bool fase10;
    public bool fase11;
    public bool fase12;
    public bool fase13;
    public bool fase14;



    [Header("Fases")]
    public static int fase;
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("fase"))
        {
            fase = PlayerPrefs.GetInt("fase");
        }
        else
        {
            fase = 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        Fases();
    }

    public void Fases()
    {
        if (fase >= 1)
        {
            bf1.SetActive(true);
        }
        else
        {
            bf1.SetActive(false);
        }

        if (fase >= 2)
        {
            bf2.SetActive(true);
        }
        else
        {
            bf2.SetActive(false);
        }

        if (fase >= 3)
        {
            bf3.SetActive(true);
        }
        else
        {
            bf3.SetActive(false);
        }

        if (fase >= 4)
        {
            bfb.SetActive(true);
        }
        else
        {
            bfb.SetActive(false);
        }

        if (fase >= 5)
        {
            bf5.SetActive(true);
        }
        else
        {
            bf5.SetActive(false);
        }

        if (fase >= 6)
        {
            bf6.SetActive(true);
        }
        else
        {
            bf6.SetActive(false);
        }

        if (fase >= 7)
        {
            bf7.SetActive(true);
        }
        else
        {
            bf7.SetActive(false);
        }

        if (fase >= 8)
        {
            bf8.SetActive(true);
        }
        else
        {
            bf8.SetActive(false);
        }

        if (fase >= 9)
        {
            bfb2.SetActive(true);
        }
        else
        {
            bfb2.SetActive(false);
        }

        if (fase >= 10)
        {
            bf10.SetActive(true);
        }
        else
        {
            bf10.SetActive(false);
        }

        if (fase >= 11)
        {
            bf11.SetActive(true);
        }
        else
        {
            bf11.SetActive(false);
        }

        if (fase >= 12)
        {
            bf12.SetActive(true);
        }
        else
        {
            bf12.SetActive(false);
        }

        if (fase >= 13)
        {
            bf13.SetActive(true);
        }
        else
        {
            bf13.SetActive(false);
        }

        if (fase >= 14)
        {
            bfb3.SetActive(true);
        }
        else
        {
            bfb3.SetActive(false);
        }
    }

    public void Fase1()
    {
        Som[0].Play();
        SceneManager.LoadScene("RoteiroTutorial");
    }

    public void Fase2()
    {
        Som[0].Play();
        SceneManager.LoadScene("RoteiroFase1");
    }

    public void Fase3()
    {
        Som[0].Play();
        SceneManager.LoadScene("RoteiroFase2");
    }

    public void FaseBoss()
    {
        Som[0].Play();
        SceneManager.LoadScene("RoteiroFase3");
    }

    public void Fase5()
    {
        Som[0].Play();
        SceneManager.LoadScene("2x2Fase2");
    }

    public void Fase6()
    {
        Som[0].Play();
        SceneManager.LoadScene("3x3");
    }

    public void Fase7()
    {
        Som[0].Play();
        SceneManager.LoadScene("3x32");
    }
    public void Fase8()
    {
        Som[0].Play();
        SceneManager.LoadScene("3x33");
    }

    public void FaseBoss2()
    {
        Som[0].Play();
        SceneManager.LoadScene("RoteiroBoss2");
    }

    public void Fase10()
    {
        Som[0].Play();
        SceneManager.LoadScene("RoteiroFase10");
    }

    public void Fase11()
    {
        Som[0].Play();
        SceneManager.LoadScene("3x35");
    }

    public void Fase12()
    {
        Som[0].Play();
        SceneManager.LoadScene("3x36");
    }
    public void Fase13()
    {
        Som[0].Play();
        SceneManager.LoadScene("RoteiroFase13");
    }

    public void FaseBoss3()
    {
        Som[0].Play();
        SceneManager.LoadScene("BossFase3");
    }

    public void Voltar()
    {
        Som[0].Play();
        SceneManager.LoadScene("Menu");
    }

}
