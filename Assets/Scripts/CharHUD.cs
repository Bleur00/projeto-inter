using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharHUD : MonoBehaviour
{
    public Slider hpSlider;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetHUD(Personagens hud)
    {
        hpSlider.maxValue = hud.maxHp;
        hpSlider.value = hud.atualHp;
    }

    public void SetHP(int hp)
    {
        hpSlider.value = hp;
    }
}
